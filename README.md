# Replication kit for simulating live cloud adaptations prior to a production deployment using a models at runtime approach
This repository contains the information necessary to execute the case study described in our paper simulating live cloud adaptations prior to a production deployment using a models at runtime approach.

Moreover, this repository contains an OCCI extension which provides simulation tags to be used within connectors fitting the OCCIWare ecosystem.
The tags are used to control the degree to which an entitiy should be simulated.
Combined, these tags are used to create a shrinked but local replication of the production environment.
Controlling individual simulation levels allows among others to actually develop and test configuration management scripts in a simulated environment.

#Getting Started
### Install the tooling
1. Download the SmartWYRM docker image:
   ```bash
     docker pull erbel/smartwyrm-sim:1.0.1
   ```
2. Start the image:
   ```bash
   docker run --privileged -p 8080:8080 -p 8081:8081 erbel/smartwyrm-sim:1.0.1
   ```
3. Open SmartWYRM in a browser at [localhost:8081](localhost:8081)

### Load and deploy the transformed production model
In this step, an already transformed model of the production environment is provided with predefined simulation levels.
While the master and one worker is actually deployed locally, the other worker is only simulated on a model-based level.
1. Click on the model store on the left.
2. Load the hadoop\_prod\_transformed model as design time model by clicking on the three dots and selecting "choose as deployment"
3. After the model has loaded click on deploy to deploy it.
   The runtime model should now change according to the new deployment model.

![sim](doc/SmartWYRM2.webm)

### Load and deploy the spark production model
We adapted this model to contain an additional component (spark).
The configuration of spark for hadoop can be tested and deployed locally via the SmartWYRM webinterface.
We already included the component and necessary configuration in an additional model that we can directly load now.

1. Click on the Model store on the left.
2. Load the spark\_dev model as design time model by clicking on the three dots and selecting "choose as deployment"
3. After the model has loaded click on deploy to deploy it.
   The runtime model should now change according to the new deployment model.
4. This adds a new spark component which can be configured (this is already taken care of in the replication kit).
5. Select the new component and click on start to deploy and start it.
    1. This deploys spark on the existing hadoop deployment.


#### Load and deploy the changed production model
Finally, we tested the behaviour of our adjusted model in an orchestrated environment.
Therefore, we added sensors to the model providing monitoring information and adjusted a previously developed self-adaptive control loop horizontally scaling the nodes in the runtime model.
1. Click on the model store on the left.
2. Load the spark\_dev\_orch model as design time model by clicking on the three dots and selecting "choose as deployment"
3. After the model has loaded click on deploy to deploy it.
4. This directly adds an starts sensor components to the runtime model.
5. The added components are able to monitor CPU and scale the number of workers accordingly (simulated in this replication kit).

## Simulation Level
To setup the simulation level, an attribute can be simply changed by the user in the runtime model.
A level of 0 indicates a simulation on the model-based level, i.e., only state changes are triggered.
The higher the level, the more simulation behaviour is added. A simulation level of one, e.g., adds artificial timing
to the lifecycle actions of the entity. Among others, this allows developers to observe changes of the runtime model more easily.
Finally, on a level of three an actual deployment and provisioning of tagged elements are performed on a local workstation.
 
To allow for a local deployment simulation multiple implementations are utilized.
In the following we provide a list of different effectors adjusted to incorporate simulation behaviour,
 as well as the differnt simulation levels.

|**SimulationTag**     | **Level**  | **Simulation**  | **Implementation**|
| ------------- |-------------| -------------|-------------|
| SimulationTag(Default) | 0 | CRUD operations + State changes |
| ComputeSim      | 1      | Local provisioning | [Docker](https://github.com/jerbel/Docker-Studio) effector|
| SensorSim   | 1 | Generated monitoring results | [MOCCI](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mocci) effector|
| ComponentSim | 1      | Lifecycle action time | [MoDMaCAO](https://github.com/jerbel/MoDMaCAO) effector|
|  | 2      | Artificial workload | |
|  | 3      | Local deployment | |


*Note:* The simulation levels and used tags are by no means exhaustive and are subject to change as they can be easily extended.
Furthermore, the effectors in the OCCIWare ecosystem are called connectors and can be find within the linked projects.

### Further Implementations
In addition to the simulation tag extension and the effector implemnetations described above, multiple other implementations
got utilized to perform a local deployment.

1. [OCCIWare Studio](https://github.com/occiware/OCCI-Studio) is a model-driven tool chain to generate OCCI extension and was used to generate the simulation tag extension.
2. [OCCIWare Runtime](https://github.com/occiware/MartServer) (Martserver) is server application which maintains the runtime model according to registered extensions and effectors.
3. [SmartWYRM](https://gitlab.gwdg.de/rwm/smartwyrm) is a web server application providing a graphical interface to deploy and manage OCCI runtime models.
4. [TOCCI](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.tocci) provides a set of model to model transformations including the transformation from a production to a local runtime model.

/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package de.ugoe.cs.rwm.domain.workload;

import org.eclipse.cmf.occi.core.MixinBase;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Computesim</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.Computesim#getSimVirtualization <em>Sim Virtualization</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.Computesim#getSimImage <em>Sim Image</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.Computesim#getSimOptions <em>Sim Options</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.rwm.domain.workload.WorkloadPackage#getComputesim()
 * @model
 * @generated
 */
public interface Computesim extends Simulation, MixinBase {
	/**
	 * Returns the value of the '<em><b>Sim Virtualization</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Local virtualization type to be chosen, e.g.: Docker, VM
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Sim Virtualization</em>' attribute.
	 * @see #setSimVirtualization(String)
	 * @see de.ugoe.cs.rwm.domain.workload.WorkloadPackage#getComputesim_SimVirtualization()
	 * @model dataType="de.ugoe.cs.rwm.domain.workload.String"
	 * @generated
	 */
	String getSimVirtualization();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.workload.Computesim#getSimVirtualization <em>Sim Virtualization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sim Virtualization</em>' attribute.
	 * @see #getSimVirtualization()
	 * @generated
	 */
	void setSimVirtualization(String value);

	/**
	 * Returns the value of the '<em><b>Sim Image</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Image to be used by the virtualization simulation.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Sim Image</em>' attribute.
	 * @see #setSimImage(String)
	 * @see de.ugoe.cs.rwm.domain.workload.WorkloadPackage#getComputesim_SimImage()
	 * @model dataType="de.ugoe.cs.rwm.domain.workload.String"
	 * @generated
	 */
	String getSimImage();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.workload.Computesim#getSimImage <em>Sim Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sim Image</em>' attribute.
	 * @see #getSimImage()
	 * @generated
	 */
	void setSimImage(String value);

	/**
	 * Returns the value of the '<em><b>Sim Options</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Additional options to start the virtualized environment
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Sim Options</em>' attribute.
	 * @see #setSimOptions(String)
	 * @see de.ugoe.cs.rwm.domain.workload.WorkloadPackage#getComputesim_SimOptions()
	 * @model dataType="de.ugoe.cs.rwm.domain.workload.String"
	 * @generated
	 */
	String getSimOptions();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.workload.Computesim#getSimOptions <em>Sim Options</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sim Options</em>' attribute.
	 * @see #getSimOptions()
	 * @generated
	 */
	void setSimOptions(String value);

} // Computesim

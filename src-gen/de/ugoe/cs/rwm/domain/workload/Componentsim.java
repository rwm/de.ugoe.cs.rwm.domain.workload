/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package de.ugoe.cs.rwm.domain.workload;

import org.eclipse.cmf.occi.core.MixinBase;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Componentsim</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.Componentsim#getSimLifecycleTimeMin <em>Sim Lifecycle Time Min</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.Componentsim#getSimLifecycleTimeMax <em>Sim Lifecycle Time Max</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.rwm.domain.workload.WorkloadPackage#getComponentsim()
 * @model
 * @generated
 */
public interface Componentsim extends Simulation, MixinBase {
	/**
	 * Returns the value of the '<em><b>Sim Lifecycle Time Min</b></em>' attribute.
	 * The default value is <code>"1000"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Sim Lifecycle Time Min</em>' attribute.
	 * @see #setSimLifecycleTimeMin(Integer)
	 * @see de.ugoe.cs.rwm.domain.workload.WorkloadPackage#getComponentsim_SimLifecycleTimeMin()
	 * @model default="1000" dataType="de.ugoe.cs.rwm.domain.workload.Int"
	 * @generated
	 */
	Integer getSimLifecycleTimeMin();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.workload.Componentsim#getSimLifecycleTimeMin <em>Sim Lifecycle Time Min</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sim Lifecycle Time Min</em>' attribute.
	 * @see #getSimLifecycleTimeMin()
	 * @generated
	 */
	void setSimLifecycleTimeMin(Integer value);

	/**
	 * Returns the value of the '<em><b>Sim Lifecycle Time Max</b></em>' attribute.
	 * The default value is <code>"3000"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Sim Lifecycle Time Max</em>' attribute.
	 * @see #setSimLifecycleTimeMax(Integer)
	 * @see de.ugoe.cs.rwm.domain.workload.WorkloadPackage#getComponentsim_SimLifecycleTimeMax()
	 * @model default="3000" dataType="de.ugoe.cs.rwm.domain.workload.Int"
	 * @generated
	 */
	Integer getSimLifecycleTimeMax();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.workload.Componentsim#getSimLifecycleTimeMax <em>Sim Lifecycle Time Max</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sim Lifecycle Time Max</em>' attribute.
	 * @see #getSimLifecycleTimeMax()
	 * @generated
	 */
	void setSimLifecycleTimeMax(Integer value);

} // Componentsim

/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package de.ugoe.cs.rwm.domain.workload;

import modmacao.Component;

import org.eclipse.cmf.occi.core.MixinBase;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Loadsimulator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorTimeout <em>Load Simulator Timeout</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorCpu <em>Load Simulator Cpu</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorIo <em>Load Simulator Io</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorVm <em>Load Simulator Vm</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorVmBytes <em>Load Simulator Vm Bytes</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorVmStride <em>Load Simulator Vm Stride</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorVmHang <em>Load Simulator Vm Hang</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorVmKeep <em>Load Simulator Vm Keep</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorHdd <em>Load Simulator Hdd</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorHddBytes <em>Load Simulator Hdd Bytes</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.rwm.domain.workload.WorkloadPackage#getLoadsimulator()
 * @model
 * @generated
 */
public interface Loadsimulator extends Component, MixinBase {
	/**
	 * Returns the value of the '<em><b>Load Simulator Timeout</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Load Simulator Timeout</em>' attribute.
	 * @see #setLoadSimulatorTimeout(String)
	 * @see de.ugoe.cs.rwm.domain.workload.WorkloadPackage#getLoadsimulator_LoadSimulatorTimeout()
	 * @model dataType="de.ugoe.cs.rwm.domain.workload.String"
	 * @generated
	 */
	String getLoadSimulatorTimeout();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorTimeout <em>Load Simulator Timeout</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Load Simulator Timeout</em>' attribute.
	 * @see #getLoadSimulatorTimeout()
	 * @generated
	 */
	void setLoadSimulatorTimeout(String value);

	/**
	 * Returns the value of the '<em><b>Load Simulator Cpu</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Load Simulator Cpu</em>' attribute.
	 * @see #setLoadSimulatorCpu(Integer)
	 * @see de.ugoe.cs.rwm.domain.workload.WorkloadPackage#getLoadsimulator_LoadSimulatorCpu()
	 * @model dataType="de.ugoe.cs.rwm.domain.workload.Int"
	 * @generated
	 */
	Integer getLoadSimulatorCpu();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorCpu <em>Load Simulator Cpu</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Load Simulator Cpu</em>' attribute.
	 * @see #getLoadSimulatorCpu()
	 * @generated
	 */
	void setLoadSimulatorCpu(Integer value);

	/**
	 * Returns the value of the '<em><b>Load Simulator Io</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Load Simulator Io</em>' attribute.
	 * @see #setLoadSimulatorIo(Integer)
	 * @see de.ugoe.cs.rwm.domain.workload.WorkloadPackage#getLoadsimulator_LoadSimulatorIo()
	 * @model dataType="de.ugoe.cs.rwm.domain.workload.Int"
	 * @generated
	 */
	Integer getLoadSimulatorIo();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorIo <em>Load Simulator Io</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Load Simulator Io</em>' attribute.
	 * @see #getLoadSimulatorIo()
	 * @generated
	 */
	void setLoadSimulatorIo(Integer value);

	/**
	 * Returns the value of the '<em><b>Load Simulator Vm</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Load Simulator Vm</em>' attribute.
	 * @see #setLoadSimulatorVm(Integer)
	 * @see de.ugoe.cs.rwm.domain.workload.WorkloadPackage#getLoadsimulator_LoadSimulatorVm()
	 * @model dataType="de.ugoe.cs.rwm.domain.workload.Int"
	 * @generated
	 */
	Integer getLoadSimulatorVm();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorVm <em>Load Simulator Vm</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Load Simulator Vm</em>' attribute.
	 * @see #getLoadSimulatorVm()
	 * @generated
	 */
	void setLoadSimulatorVm(Integer value);

	/**
	 * Returns the value of the '<em><b>Load Simulator Vm Bytes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Load Simulator Vm Bytes</em>' attribute.
	 * @see #setLoadSimulatorVmBytes(String)
	 * @see de.ugoe.cs.rwm.domain.workload.WorkloadPackage#getLoadsimulator_LoadSimulatorVmBytes()
	 * @model dataType="de.ugoe.cs.rwm.domain.workload.String"
	 * @generated
	 */
	String getLoadSimulatorVmBytes();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorVmBytes <em>Load Simulator Vm Bytes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Load Simulator Vm Bytes</em>' attribute.
	 * @see #getLoadSimulatorVmBytes()
	 * @generated
	 */
	void setLoadSimulatorVmBytes(String value);

	/**
	 * Returns the value of the '<em><b>Load Simulator Vm Stride</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Load Simulator Vm Stride</em>' attribute.
	 * @see #setLoadSimulatorVmStride(String)
	 * @see de.ugoe.cs.rwm.domain.workload.WorkloadPackage#getLoadsimulator_LoadSimulatorVmStride()
	 * @model dataType="de.ugoe.cs.rwm.domain.workload.String"
	 * @generated
	 */
	String getLoadSimulatorVmStride();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorVmStride <em>Load Simulator Vm Stride</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Load Simulator Vm Stride</em>' attribute.
	 * @see #getLoadSimulatorVmStride()
	 * @generated
	 */
	void setLoadSimulatorVmStride(String value);

	/**
	 * Returns the value of the '<em><b>Load Simulator Vm Hang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Load Simulator Vm Hang</em>' attribute.
	 * @see #setLoadSimulatorVmHang(String)
	 * @see de.ugoe.cs.rwm.domain.workload.WorkloadPackage#getLoadsimulator_LoadSimulatorVmHang()
	 * @model dataType="de.ugoe.cs.rwm.domain.workload.String"
	 * @generated
	 */
	String getLoadSimulatorVmHang();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorVmHang <em>Load Simulator Vm Hang</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Load Simulator Vm Hang</em>' attribute.
	 * @see #getLoadSimulatorVmHang()
	 * @generated
	 */
	void setLoadSimulatorVmHang(String value);

	/**
	 * Returns the value of the '<em><b>Load Simulator Vm Keep</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Load Simulator Vm Keep</em>' attribute.
	 * @see #setLoadSimulatorVmKeep(String)
	 * @see de.ugoe.cs.rwm.domain.workload.WorkloadPackage#getLoadsimulator_LoadSimulatorVmKeep()
	 * @model dataType="de.ugoe.cs.rwm.domain.workload.String"
	 * @generated
	 */
	String getLoadSimulatorVmKeep();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorVmKeep <em>Load Simulator Vm Keep</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Load Simulator Vm Keep</em>' attribute.
	 * @see #getLoadSimulatorVmKeep()
	 * @generated
	 */
	void setLoadSimulatorVmKeep(String value);

	/**
	 * Returns the value of the '<em><b>Load Simulator Hdd</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Load Simulator Hdd</em>' attribute.
	 * @see #setLoadSimulatorHdd(Integer)
	 * @see de.ugoe.cs.rwm.domain.workload.WorkloadPackage#getLoadsimulator_LoadSimulatorHdd()
	 * @model dataType="de.ugoe.cs.rwm.domain.workload.Int"
	 * @generated
	 */
	Integer getLoadSimulatorHdd();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorHdd <em>Load Simulator Hdd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Load Simulator Hdd</em>' attribute.
	 * @see #getLoadSimulatorHdd()
	 * @generated
	 */
	void setLoadSimulatorHdd(Integer value);

	/**
	 * Returns the value of the '<em><b>Load Simulator Hdd Bytes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Load Simulator Hdd Bytes</em>' attribute.
	 * @see #setLoadSimulatorHddBytes(String)
	 * @see de.ugoe.cs.rwm.domain.workload.WorkloadPackage#getLoadsimulator_LoadSimulatorHddBytes()
	 * @model dataType="de.ugoe.cs.rwm.domain.workload.String"
	 * @generated
	 */
	String getLoadSimulatorHddBytes();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorHddBytes <em>Load Simulator Hdd Bytes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Load Simulator Hdd Bytes</em>' attribute.
	 * @see #getLoadSimulatorHddBytes()
	 * @generated
	 */
	void setLoadSimulatorHddBytes(String value);

} // Loadsimulator

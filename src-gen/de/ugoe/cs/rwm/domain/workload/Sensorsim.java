/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package de.ugoe.cs.rwm.domain.workload;

import org.eclipse.cmf.occi.core.MixinBase;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sensorsim</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.Sensorsim#getSimChangeRate <em>Sim Change Rate</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.Sensorsim#getSimMonitoringResults <em>Sim Monitoring Results</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.rwm.domain.workload.WorkloadPackage#getSensorsim()
 * @model
 * @generated
 */
public interface Sensorsim extends Simulation, MixinBase {
	/**
	 * Returns the value of the '<em><b>Sim Change Rate</b></em>' attribute.
	 * The default value is <code>"2000"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * 
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Sim Change Rate</em>' attribute.
	 * @see #setSimChangeRate(Integer)
	 * @see de.ugoe.cs.rwm.domain.workload.WorkloadPackage#getSensorsim_SimChangeRate()
	 * @model default="2000" dataType="de.ugoe.cs.rwm.domain.workload.Int"
	 * @generated
	 */
	Integer getSimChangeRate();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.workload.Sensorsim#getSimChangeRate <em>Sim Change Rate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sim Change Rate</em>' attribute.
	 * @see #getSimChangeRate()
	 * @generated
	 */
	void setSimChangeRate(Integer value);

	/**
	 * Returns the value of the '<em><b>Sim Monitoring Results</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Comma separated list of possible monitoring outcomes
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Sim Monitoring Results</em>' attribute.
	 * @see #setSimMonitoringResults(String)
	 * @see de.ugoe.cs.rwm.domain.workload.WorkloadPackage#getSensorsim_SimMonitoringResults()
	 * @model dataType="de.ugoe.cs.rwm.domain.workload.String"
	 * @generated
	 */
	String getSimMonitoringResults();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.workload.Sensorsim#getSimMonitoringResults <em>Sim Monitoring Results</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sim Monitoring Results</em>' attribute.
	 * @see #getSimMonitoringResults()
	 * @generated
	 */
	void setSimMonitoringResults(String value);

} // Sensorsim

/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package de.ugoe.cs.rwm.domain.workload;

import modmacao.ModmacaoPackage;

import org.eclipse.cmf.occi.core.OCCIPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.ugoe.cs.rwm.domain.workload.WorkloadFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore"
 * @generated
 */
public interface WorkloadPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "workload";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://schemas.ugoe.cs.rwm/domain/workload/ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "workload";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	WorkloadPackage eINSTANCE = de.ugoe.cs.rwm.domain.workload.impl.WorkloadPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.ugoe.cs.rwm.domain.workload.impl.LoadsimulatorImpl <em>Loadsimulator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.ugoe.cs.rwm.domain.workload.impl.LoadsimulatorImpl
	 * @see de.ugoe.cs.rwm.domain.workload.impl.WorkloadPackageImpl#getLoadsimulator()
	 * @generated
	 */
	int LOADSIMULATOR = 0;

	/**
	 * The feature id for the '<em><b>Mixin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOADSIMULATOR__MIXIN = ModmacaoPackage.COMPONENT__MIXIN;

	/**
	 * The feature id for the '<em><b>Entity</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOADSIMULATOR__ENTITY = ModmacaoPackage.COMPONENT__ENTITY;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOADSIMULATOR__ATTRIBUTES = ModmacaoPackage.COMPONENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Modmacao Component Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOADSIMULATOR__MODMACAO_COMPONENT_VERSION = ModmacaoPackage.COMPONENT__MODMACAO_COMPONENT_VERSION;

	/**
	 * The feature id for the '<em><b>Load Simulator Timeout</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOADSIMULATOR__LOAD_SIMULATOR_TIMEOUT = ModmacaoPackage.COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Load Simulator Cpu</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOADSIMULATOR__LOAD_SIMULATOR_CPU = ModmacaoPackage.COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Load Simulator Io</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOADSIMULATOR__LOAD_SIMULATOR_IO = ModmacaoPackage.COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Load Simulator Vm</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOADSIMULATOR__LOAD_SIMULATOR_VM = ModmacaoPackage.COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Load Simulator Vm Bytes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOADSIMULATOR__LOAD_SIMULATOR_VM_BYTES = ModmacaoPackage.COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Load Simulator Vm Stride</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOADSIMULATOR__LOAD_SIMULATOR_VM_STRIDE = ModmacaoPackage.COMPONENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Load Simulator Vm Hang</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOADSIMULATOR__LOAD_SIMULATOR_VM_HANG = ModmacaoPackage.COMPONENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Load Simulator Vm Keep</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOADSIMULATOR__LOAD_SIMULATOR_VM_KEEP = ModmacaoPackage.COMPONENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Load Simulator Hdd</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOADSIMULATOR__LOAD_SIMULATOR_HDD = ModmacaoPackage.COMPONENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Load Simulator Hdd Bytes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOADSIMULATOR__LOAD_SIMULATOR_HDD_BYTES = ModmacaoPackage.COMPONENT_FEATURE_COUNT + 9;

	/**
	 * The number of structural features of the '<em>Loadsimulator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOADSIMULATOR_FEATURE_COUNT = ModmacaoPackage.COMPONENT_FEATURE_COUNT + 10;

	/**
	 * The number of operations of the '<em>Loadsimulator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOADSIMULATOR_OPERATION_COUNT = ModmacaoPackage.COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.ugoe.cs.rwm.domain.workload.impl.SimulationImpl <em>Simulation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.ugoe.cs.rwm.domain.workload.impl.SimulationImpl
	 * @see de.ugoe.cs.rwm.domain.workload.impl.WorkloadPackageImpl#getSimulation()
	 * @generated
	 */
	int SIMULATION = 1;

	/**
	 * The feature id for the '<em><b>Mixin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION__MIXIN = OCCIPackage.MIXIN_BASE__MIXIN;

	/**
	 * The feature id for the '<em><b>Entity</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION__ENTITY = OCCIPackage.MIXIN_BASE__ENTITY;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION__ATTRIBUTES = OCCIPackage.MIXIN_BASE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sim Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION__SIM_LEVEL = OCCIPackage.MIXIN_BASE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Simulation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_FEATURE_COUNT = OCCIPackage.MIXIN_BASE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Applies Constraint</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION___APPLIES_CONSTRAINT__DIAGNOSTICCHAIN_MAP = OCCIPackage.MIXIN_BASE_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Simulation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_OPERATION_COUNT = OCCIPackage.MIXIN_BASE_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.ugoe.cs.rwm.domain.workload.impl.ComputesimImpl <em>Computesim</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.ugoe.cs.rwm.domain.workload.impl.ComputesimImpl
	 * @see de.ugoe.cs.rwm.domain.workload.impl.WorkloadPackageImpl#getComputesim()
	 * @generated
	 */
	int COMPUTESIM = 2;

	/**
	 * The feature id for the '<em><b>Mixin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTESIM__MIXIN = SIMULATION__MIXIN;

	/**
	 * The feature id for the '<em><b>Entity</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTESIM__ENTITY = SIMULATION__ENTITY;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTESIM__ATTRIBUTES = SIMULATION__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sim Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTESIM__SIM_LEVEL = SIMULATION__SIM_LEVEL;

	/**
	 * The feature id for the '<em><b>Sim Virtualization</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTESIM__SIM_VIRTUALIZATION = SIMULATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Sim Image</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTESIM__SIM_IMAGE = SIMULATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Sim Options</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTESIM__SIM_OPTIONS = SIMULATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Computesim</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTESIM_FEATURE_COUNT = SIMULATION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Applies Constraint</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTESIM___APPLIES_CONSTRAINT__DIAGNOSTICCHAIN_MAP = SIMULATION___APPLIES_CONSTRAINT__DIAGNOSTICCHAIN_MAP;

	/**
	 * The number of operations of the '<em>Computesim</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTESIM_OPERATION_COUNT = SIMULATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.ugoe.cs.rwm.domain.workload.impl.ComponentsimImpl <em>Componentsim</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.ugoe.cs.rwm.domain.workload.impl.ComponentsimImpl
	 * @see de.ugoe.cs.rwm.domain.workload.impl.WorkloadPackageImpl#getComponentsim()
	 * @generated
	 */
	int COMPONENTSIM = 3;

	/**
	 * The feature id for the '<em><b>Mixin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENTSIM__MIXIN = SIMULATION__MIXIN;

	/**
	 * The feature id for the '<em><b>Entity</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENTSIM__ENTITY = SIMULATION__ENTITY;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENTSIM__ATTRIBUTES = SIMULATION__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sim Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENTSIM__SIM_LEVEL = SIMULATION__SIM_LEVEL;

	/**
	 * The feature id for the '<em><b>Sim Lifecycle Time Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENTSIM__SIM_LIFECYCLE_TIME_MIN = SIMULATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Sim Lifecycle Time Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENTSIM__SIM_LIFECYCLE_TIME_MAX = SIMULATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Componentsim</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENTSIM_FEATURE_COUNT = SIMULATION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Applies Constraint</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENTSIM___APPLIES_CONSTRAINT__DIAGNOSTICCHAIN_MAP = SIMULATION___APPLIES_CONSTRAINT__DIAGNOSTICCHAIN_MAP;

	/**
	 * The number of operations of the '<em>Componentsim</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENTSIM_OPERATION_COUNT = SIMULATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.ugoe.cs.rwm.domain.workload.impl.SensorsimImpl <em>Sensorsim</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.ugoe.cs.rwm.domain.workload.impl.SensorsimImpl
	 * @see de.ugoe.cs.rwm.domain.workload.impl.WorkloadPackageImpl#getSensorsim()
	 * @generated
	 */
	int SENSORSIM = 4;

	/**
	 * The feature id for the '<em><b>Mixin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSORSIM__MIXIN = SIMULATION__MIXIN;

	/**
	 * The feature id for the '<em><b>Entity</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSORSIM__ENTITY = SIMULATION__ENTITY;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSORSIM__ATTRIBUTES = SIMULATION__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sim Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSORSIM__SIM_LEVEL = SIMULATION__SIM_LEVEL;

	/**
	 * The feature id for the '<em><b>Sim Change Rate</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSORSIM__SIM_CHANGE_RATE = SIMULATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Sim Monitoring Results</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSORSIM__SIM_MONITORING_RESULTS = SIMULATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Sensorsim</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSORSIM_FEATURE_COUNT = SIMULATION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Applies Constraint</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSORSIM___APPLIES_CONSTRAINT__DIAGNOSTICCHAIN_MAP = SIMULATION___APPLIES_CONSTRAINT__DIAGNOSTICCHAIN_MAP;

	/**
	 * The number of operations of the '<em>Sensorsim</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSORSIM_OPERATION_COUNT = SIMULATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '<em>String</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see de.ugoe.cs.rwm.domain.workload.impl.WorkloadPackageImpl#getString()
	 * @generated
	 */
	int STRING = 5;

	/**
	 * The meta object id for the '<em>Int</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.Integer
	 * @see de.ugoe.cs.rwm.domain.workload.impl.WorkloadPackageImpl#getInt()
	 * @generated
	 */
	int INT = 6;


	/**
	 * Returns the meta object for class '{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator <em>Loadsimulator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Loadsimulator</em>'.
	 * @see de.ugoe.cs.rwm.domain.workload.Loadsimulator
	 * @generated
	 */
	EClass getLoadsimulator();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorTimeout <em>Load Simulator Timeout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Load Simulator Timeout</em>'.
	 * @see de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorTimeout()
	 * @see #getLoadsimulator()
	 * @generated
	 */
	EAttribute getLoadsimulator_LoadSimulatorTimeout();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorCpu <em>Load Simulator Cpu</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Load Simulator Cpu</em>'.
	 * @see de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorCpu()
	 * @see #getLoadsimulator()
	 * @generated
	 */
	EAttribute getLoadsimulator_LoadSimulatorCpu();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorIo <em>Load Simulator Io</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Load Simulator Io</em>'.
	 * @see de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorIo()
	 * @see #getLoadsimulator()
	 * @generated
	 */
	EAttribute getLoadsimulator_LoadSimulatorIo();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorVm <em>Load Simulator Vm</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Load Simulator Vm</em>'.
	 * @see de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorVm()
	 * @see #getLoadsimulator()
	 * @generated
	 */
	EAttribute getLoadsimulator_LoadSimulatorVm();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorVmBytes <em>Load Simulator Vm Bytes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Load Simulator Vm Bytes</em>'.
	 * @see de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorVmBytes()
	 * @see #getLoadsimulator()
	 * @generated
	 */
	EAttribute getLoadsimulator_LoadSimulatorVmBytes();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorVmStride <em>Load Simulator Vm Stride</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Load Simulator Vm Stride</em>'.
	 * @see de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorVmStride()
	 * @see #getLoadsimulator()
	 * @generated
	 */
	EAttribute getLoadsimulator_LoadSimulatorVmStride();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorVmHang <em>Load Simulator Vm Hang</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Load Simulator Vm Hang</em>'.
	 * @see de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorVmHang()
	 * @see #getLoadsimulator()
	 * @generated
	 */
	EAttribute getLoadsimulator_LoadSimulatorVmHang();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorVmKeep <em>Load Simulator Vm Keep</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Load Simulator Vm Keep</em>'.
	 * @see de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorVmKeep()
	 * @see #getLoadsimulator()
	 * @generated
	 */
	EAttribute getLoadsimulator_LoadSimulatorVmKeep();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorHdd <em>Load Simulator Hdd</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Load Simulator Hdd</em>'.
	 * @see de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorHdd()
	 * @see #getLoadsimulator()
	 * @generated
	 */
	EAttribute getLoadsimulator_LoadSimulatorHdd();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorHddBytes <em>Load Simulator Hdd Bytes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Load Simulator Hdd Bytes</em>'.
	 * @see de.ugoe.cs.rwm.domain.workload.Loadsimulator#getLoadSimulatorHddBytes()
	 * @see #getLoadsimulator()
	 * @generated
	 */
	EAttribute getLoadsimulator_LoadSimulatorHddBytes();

	/**
	 * Returns the meta object for class '{@link de.ugoe.cs.rwm.domain.workload.Simulation <em>Simulation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simulation</em>'.
	 * @see de.ugoe.cs.rwm.domain.workload.Simulation
	 * @generated
	 */
	EClass getSimulation();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.workload.Simulation#getSimLevel <em>Sim Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sim Level</em>'.
	 * @see de.ugoe.cs.rwm.domain.workload.Simulation#getSimLevel()
	 * @see #getSimulation()
	 * @generated
	 */
	EAttribute getSimulation_SimLevel();

	/**
	 * Returns the meta object for the '{@link de.ugoe.cs.rwm.domain.workload.Simulation#appliesConstraint(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Applies Constraint</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Applies Constraint</em>' operation.
	 * @see de.ugoe.cs.rwm.domain.workload.Simulation#appliesConstraint(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getSimulation__AppliesConstraint__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link de.ugoe.cs.rwm.domain.workload.Computesim <em>Computesim</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Computesim</em>'.
	 * @see de.ugoe.cs.rwm.domain.workload.Computesim
	 * @generated
	 */
	EClass getComputesim();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.workload.Computesim#getSimVirtualization <em>Sim Virtualization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sim Virtualization</em>'.
	 * @see de.ugoe.cs.rwm.domain.workload.Computesim#getSimVirtualization()
	 * @see #getComputesim()
	 * @generated
	 */
	EAttribute getComputesim_SimVirtualization();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.workload.Computesim#getSimImage <em>Sim Image</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sim Image</em>'.
	 * @see de.ugoe.cs.rwm.domain.workload.Computesim#getSimImage()
	 * @see #getComputesim()
	 * @generated
	 */
	EAttribute getComputesim_SimImage();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.workload.Computesim#getSimOptions <em>Sim Options</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sim Options</em>'.
	 * @see de.ugoe.cs.rwm.domain.workload.Computesim#getSimOptions()
	 * @see #getComputesim()
	 * @generated
	 */
	EAttribute getComputesim_SimOptions();

	/**
	 * Returns the meta object for class '{@link de.ugoe.cs.rwm.domain.workload.Componentsim <em>Componentsim</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Componentsim</em>'.
	 * @see de.ugoe.cs.rwm.domain.workload.Componentsim
	 * @generated
	 */
	EClass getComponentsim();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.workload.Componentsim#getSimLifecycleTimeMin <em>Sim Lifecycle Time Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sim Lifecycle Time Min</em>'.
	 * @see de.ugoe.cs.rwm.domain.workload.Componentsim#getSimLifecycleTimeMin()
	 * @see #getComponentsim()
	 * @generated
	 */
	EAttribute getComponentsim_SimLifecycleTimeMin();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.workload.Componentsim#getSimLifecycleTimeMax <em>Sim Lifecycle Time Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sim Lifecycle Time Max</em>'.
	 * @see de.ugoe.cs.rwm.domain.workload.Componentsim#getSimLifecycleTimeMax()
	 * @see #getComponentsim()
	 * @generated
	 */
	EAttribute getComponentsim_SimLifecycleTimeMax();

	/**
	 * Returns the meta object for class '{@link de.ugoe.cs.rwm.domain.workload.Sensorsim <em>Sensorsim</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sensorsim</em>'.
	 * @see de.ugoe.cs.rwm.domain.workload.Sensorsim
	 * @generated
	 */
	EClass getSensorsim();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.workload.Sensorsim#getSimChangeRate <em>Sim Change Rate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sim Change Rate</em>'.
	 * @see de.ugoe.cs.rwm.domain.workload.Sensorsim#getSimChangeRate()
	 * @see #getSensorsim()
	 * @generated
	 */
	EAttribute getSensorsim_SimChangeRate();

	/**
	 * Returns the meta object for the attribute '{@link de.ugoe.cs.rwm.domain.workload.Sensorsim#getSimMonitoringResults <em>Sim Monitoring Results</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sim Monitoring Results</em>'.
	 * @see de.ugoe.cs.rwm.domain.workload.Sensorsim#getSimMonitoringResults()
	 * @see #getSensorsim()
	 * @generated
	 */
	EAttribute getSensorsim_SimMonitoringResults();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>String</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getString();

	/**
	 * Returns the meta object for data type '{@link java.lang.Integer <em>Int</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Int</em>'.
	 * @see java.lang.Integer
	 * @model instanceClass="java.lang.Integer"
	 * @generated
	 */
	EDataType getInt();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	WorkloadFactory getWorkloadFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.ugoe.cs.rwm.domain.workload.impl.LoadsimulatorImpl <em>Loadsimulator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.ugoe.cs.rwm.domain.workload.impl.LoadsimulatorImpl
		 * @see de.ugoe.cs.rwm.domain.workload.impl.WorkloadPackageImpl#getLoadsimulator()
		 * @generated
		 */
		EClass LOADSIMULATOR = eINSTANCE.getLoadsimulator();

		/**
		 * The meta object literal for the '<em><b>Load Simulator Timeout</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOADSIMULATOR__LOAD_SIMULATOR_TIMEOUT = eINSTANCE.getLoadsimulator_LoadSimulatorTimeout();

		/**
		 * The meta object literal for the '<em><b>Load Simulator Cpu</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOADSIMULATOR__LOAD_SIMULATOR_CPU = eINSTANCE.getLoadsimulator_LoadSimulatorCpu();

		/**
		 * The meta object literal for the '<em><b>Load Simulator Io</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOADSIMULATOR__LOAD_SIMULATOR_IO = eINSTANCE.getLoadsimulator_LoadSimulatorIo();

		/**
		 * The meta object literal for the '<em><b>Load Simulator Vm</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOADSIMULATOR__LOAD_SIMULATOR_VM = eINSTANCE.getLoadsimulator_LoadSimulatorVm();

		/**
		 * The meta object literal for the '<em><b>Load Simulator Vm Bytes</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOADSIMULATOR__LOAD_SIMULATOR_VM_BYTES = eINSTANCE.getLoadsimulator_LoadSimulatorVmBytes();

		/**
		 * The meta object literal for the '<em><b>Load Simulator Vm Stride</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOADSIMULATOR__LOAD_SIMULATOR_VM_STRIDE = eINSTANCE.getLoadsimulator_LoadSimulatorVmStride();

		/**
		 * The meta object literal for the '<em><b>Load Simulator Vm Hang</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOADSIMULATOR__LOAD_SIMULATOR_VM_HANG = eINSTANCE.getLoadsimulator_LoadSimulatorVmHang();

		/**
		 * The meta object literal for the '<em><b>Load Simulator Vm Keep</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOADSIMULATOR__LOAD_SIMULATOR_VM_KEEP = eINSTANCE.getLoadsimulator_LoadSimulatorVmKeep();

		/**
		 * The meta object literal for the '<em><b>Load Simulator Hdd</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOADSIMULATOR__LOAD_SIMULATOR_HDD = eINSTANCE.getLoadsimulator_LoadSimulatorHdd();

		/**
		 * The meta object literal for the '<em><b>Load Simulator Hdd Bytes</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOADSIMULATOR__LOAD_SIMULATOR_HDD_BYTES = eINSTANCE.getLoadsimulator_LoadSimulatorHddBytes();

		/**
		 * The meta object literal for the '{@link de.ugoe.cs.rwm.domain.workload.impl.SimulationImpl <em>Simulation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.ugoe.cs.rwm.domain.workload.impl.SimulationImpl
		 * @see de.ugoe.cs.rwm.domain.workload.impl.WorkloadPackageImpl#getSimulation()
		 * @generated
		 */
		EClass SIMULATION = eINSTANCE.getSimulation();

		/**
		 * The meta object literal for the '<em><b>Sim Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMULATION__SIM_LEVEL = eINSTANCE.getSimulation_SimLevel();

		/**
		 * The meta object literal for the '<em><b>Applies Constraint</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SIMULATION___APPLIES_CONSTRAINT__DIAGNOSTICCHAIN_MAP = eINSTANCE.getSimulation__AppliesConstraint__DiagnosticChain_Map();

		/**
		 * The meta object literal for the '{@link de.ugoe.cs.rwm.domain.workload.impl.ComputesimImpl <em>Computesim</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.ugoe.cs.rwm.domain.workload.impl.ComputesimImpl
		 * @see de.ugoe.cs.rwm.domain.workload.impl.WorkloadPackageImpl#getComputesim()
		 * @generated
		 */
		EClass COMPUTESIM = eINSTANCE.getComputesim();

		/**
		 * The meta object literal for the '<em><b>Sim Virtualization</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPUTESIM__SIM_VIRTUALIZATION = eINSTANCE.getComputesim_SimVirtualization();

		/**
		 * The meta object literal for the '<em><b>Sim Image</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPUTESIM__SIM_IMAGE = eINSTANCE.getComputesim_SimImage();

		/**
		 * The meta object literal for the '<em><b>Sim Options</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPUTESIM__SIM_OPTIONS = eINSTANCE.getComputesim_SimOptions();

		/**
		 * The meta object literal for the '{@link de.ugoe.cs.rwm.domain.workload.impl.ComponentsimImpl <em>Componentsim</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.ugoe.cs.rwm.domain.workload.impl.ComponentsimImpl
		 * @see de.ugoe.cs.rwm.domain.workload.impl.WorkloadPackageImpl#getComponentsim()
		 * @generated
		 */
		EClass COMPONENTSIM = eINSTANCE.getComponentsim();

		/**
		 * The meta object literal for the '<em><b>Sim Lifecycle Time Min</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPONENTSIM__SIM_LIFECYCLE_TIME_MIN = eINSTANCE.getComponentsim_SimLifecycleTimeMin();

		/**
		 * The meta object literal for the '<em><b>Sim Lifecycle Time Max</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPONENTSIM__SIM_LIFECYCLE_TIME_MAX = eINSTANCE.getComponentsim_SimLifecycleTimeMax();

		/**
		 * The meta object literal for the '{@link de.ugoe.cs.rwm.domain.workload.impl.SensorsimImpl <em>Sensorsim</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.ugoe.cs.rwm.domain.workload.impl.SensorsimImpl
		 * @see de.ugoe.cs.rwm.domain.workload.impl.WorkloadPackageImpl#getSensorsim()
		 * @generated
		 */
		EClass SENSORSIM = eINSTANCE.getSensorsim();

		/**
		 * The meta object literal for the '<em><b>Sim Change Rate</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SENSORSIM__SIM_CHANGE_RATE = eINSTANCE.getSensorsim_SimChangeRate();

		/**
		 * The meta object literal for the '<em><b>Sim Monitoring Results</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SENSORSIM__SIM_MONITORING_RESULTS = eINSTANCE.getSensorsim_SimMonitoringResults();

		/**
		 * The meta object literal for the '<em>String</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see de.ugoe.cs.rwm.domain.workload.impl.WorkloadPackageImpl#getString()
		 * @generated
		 */
		EDataType STRING = eINSTANCE.getString();

		/**
		 * The meta object literal for the '<em>Int</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.Integer
		 * @see de.ugoe.cs.rwm.domain.workload.impl.WorkloadPackageImpl#getInt()
		 * @generated
		 */
		EDataType INT = eINSTANCE.getInt();

	}

} //WorkloadPackage

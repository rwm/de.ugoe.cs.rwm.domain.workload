/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package de.ugoe.cs.rwm.domain.workload.util;

import de.ugoe.cs.rwm.domain.workload.*;

import java.util.Map;

import modmacao.util.ModmacaoValidator;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see de.ugoe.cs.rwm.domain.workload.WorkloadPackage
 * @generated
 */
public class WorkloadValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final WorkloadValidator INSTANCE = new WorkloadValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "de.ugoe.cs.rwm.domain.workload";

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Applies Constraint' of 'Simulation'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int SIMULATION__APPLIES_CONSTRAINT = 1;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 1;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * The cached base package validator.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModmacaoValidator modmacaoValidator;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorkloadValidator() {
		super();
		modmacaoValidator = ModmacaoValidator.INSTANCE;
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return WorkloadPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case WorkloadPackage.LOADSIMULATOR:
				return validateLoadsimulator((Loadsimulator)value, diagnostics, context);
			case WorkloadPackage.SIMULATION:
				return validateSimulation((Simulation)value, diagnostics, context);
			case WorkloadPackage.COMPUTESIM:
				return validateComputesim((Computesim)value, diagnostics, context);
			case WorkloadPackage.COMPONENTSIM:
				return validateComponentsim((Componentsim)value, diagnostics, context);
			case WorkloadPackage.SENSORSIM:
				return validateSensorsim((Sensorsim)value, diagnostics, context);
			case WorkloadPackage.STRING:
				return validateString((String)value, diagnostics, context);
			case WorkloadPackage.INT:
				return validateInt((Integer)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLoadsimulator(Loadsimulator loadsimulator, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(loadsimulator, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(loadsimulator, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(loadsimulator, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(loadsimulator, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(loadsimulator, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(loadsimulator, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(loadsimulator, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(loadsimulator, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(loadsimulator, diagnostics, context);
		if (result || diagnostics != null) result &= modmacaoValidator.validateComponent_OnlyOnePlacementLink(loadsimulator, diagnostics, context);
		if (result || diagnostics != null) result &= modmacaoValidator.validateComponent_appliesConstraint(loadsimulator, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSimulation(Simulation simulation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(simulation, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(simulation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(simulation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(simulation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(simulation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(simulation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(simulation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(simulation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(simulation, diagnostics, context);
		if (result || diagnostics != null) result &= validateSimulation_appliesConstraint(simulation, diagnostics, context);
		return result;
	}

	/**
	 * Validates the appliesConstraint constraint of '<em>Simulation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSimulation_appliesConstraint(Simulation simulation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return simulation.appliesConstraint(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComputesim(Computesim computesim, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(computesim, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(computesim, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(computesim, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(computesim, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(computesim, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(computesim, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(computesim, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(computesim, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(computesim, diagnostics, context);
		if (result || diagnostics != null) result &= validateSimulation_appliesConstraint(computesim, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComponentsim(Componentsim componentsim, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(componentsim, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(componentsim, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(componentsim, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(componentsim, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(componentsim, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(componentsim, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(componentsim, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(componentsim, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(componentsim, diagnostics, context);
		if (result || diagnostics != null) result &= validateSimulation_appliesConstraint(componentsim, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSensorsim(Sensorsim sensorsim, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(sensorsim, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(sensorsim, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(sensorsim, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(sensorsim, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(sensorsim, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(sensorsim, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(sensorsim, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(sensorsim, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(sensorsim, diagnostics, context);
		if (result || diagnostics != null) result &= validateSimulation_appliesConstraint(sensorsim, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateString(String string, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateInt(Integer int_, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //WorkloadValidator

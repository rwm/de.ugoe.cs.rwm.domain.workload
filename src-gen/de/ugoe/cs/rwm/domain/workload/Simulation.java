/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package de.ugoe.cs.rwm.domain.workload;

import java.util.Map;

import org.eclipse.cmf.occi.core.MixinBase;

import org.eclipse.emf.common.util.DiagnosticChain;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simulation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.Simulation#getSimLevel <em>Sim Level</em>}</li>
 * </ul>
 *
 * @see de.ugoe.cs.rwm.domain.workload.WorkloadPackage#getSimulation()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='appliesConstraint'"
 * @generated
 */
public interface Simulation extends MixinBase {
	/**
	 * Returns the value of the '<em><b>Sim Level</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Weight attribute describing the simulation factor of the attached resource.
	 * The simulation triggered depends on the weight set and how the accompanying connector of the resource interprets it.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Sim Level</em>' attribute.
	 * @see #setSimLevel(Integer)
	 * @see de.ugoe.cs.rwm.domain.workload.WorkloadPackage#getSimulation_SimLevel()
	 * @model default="0" dataType="de.ugoe.cs.rwm.domain.workload.Int" required="true"
	 * @generated
	 */
	Integer getSimLevel();

	/**
	 * Sets the value of the '{@link de.ugoe.cs.rwm.domain.workload.Simulation#getSimLevel <em>Sim Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sim Level</em>' attribute.
	 * @see #getSimLevel()
	 * @generated
	 */
	void setSimLevel(Integer value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot body='self.entity.oclIsKindOf(occi::Resource)'"
	 * @generated
	 */
	boolean appliesConstraint(DiagnosticChain diagnostics, Map<Object, Object> context);

} // Simulation

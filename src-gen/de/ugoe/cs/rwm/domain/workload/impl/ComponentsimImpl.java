/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package de.ugoe.cs.rwm.domain.workload.impl;

import de.ugoe.cs.rwm.domain.workload.Componentsim;
import de.ugoe.cs.rwm.domain.workload.WorkloadPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Componentsim</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.impl.ComponentsimImpl#getSimLifecycleTimeMin <em>Sim Lifecycle Time Min</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.impl.ComponentsimImpl#getSimLifecycleTimeMax <em>Sim Lifecycle Time Max</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComponentsimImpl extends SimulationImpl implements Componentsim {
	/**
	 * The default value of the '{@link #getSimLifecycleTimeMin() <em>Sim Lifecycle Time Min</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimLifecycleTimeMin()
	 * @generated
	 * @ordered
	 */
	protected static final Integer SIM_LIFECYCLE_TIME_MIN_EDEFAULT = new Integer(1000);

	/**
	 * The cached value of the '{@link #getSimLifecycleTimeMin() <em>Sim Lifecycle Time Min</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimLifecycleTimeMin()
	 * @generated
	 * @ordered
	 */
	protected Integer simLifecycleTimeMin = SIM_LIFECYCLE_TIME_MIN_EDEFAULT;

	/**
	 * The default value of the '{@link #getSimLifecycleTimeMax() <em>Sim Lifecycle Time Max</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimLifecycleTimeMax()
	 * @generated
	 * @ordered
	 */
	protected static final Integer SIM_LIFECYCLE_TIME_MAX_EDEFAULT = new Integer(3000);

	/**
	 * The cached value of the '{@link #getSimLifecycleTimeMax() <em>Sim Lifecycle Time Max</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimLifecycleTimeMax()
	 * @generated
	 * @ordered
	 */
	protected Integer simLifecycleTimeMax = SIM_LIFECYCLE_TIME_MAX_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComponentsimImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkloadPackage.Literals.COMPONENTSIM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Integer getSimLifecycleTimeMin() {
		return simLifecycleTimeMin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSimLifecycleTimeMin(Integer newSimLifecycleTimeMin) {
		Integer oldSimLifecycleTimeMin = simLifecycleTimeMin;
		simLifecycleTimeMin = newSimLifecycleTimeMin;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkloadPackage.COMPONENTSIM__SIM_LIFECYCLE_TIME_MIN, oldSimLifecycleTimeMin, simLifecycleTimeMin));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Integer getSimLifecycleTimeMax() {
		return simLifecycleTimeMax;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSimLifecycleTimeMax(Integer newSimLifecycleTimeMax) {
		Integer oldSimLifecycleTimeMax = simLifecycleTimeMax;
		simLifecycleTimeMax = newSimLifecycleTimeMax;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkloadPackage.COMPONENTSIM__SIM_LIFECYCLE_TIME_MAX, oldSimLifecycleTimeMax, simLifecycleTimeMax));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorkloadPackage.COMPONENTSIM__SIM_LIFECYCLE_TIME_MIN:
				return getSimLifecycleTimeMin();
			case WorkloadPackage.COMPONENTSIM__SIM_LIFECYCLE_TIME_MAX:
				return getSimLifecycleTimeMax();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorkloadPackage.COMPONENTSIM__SIM_LIFECYCLE_TIME_MIN:
				setSimLifecycleTimeMin((Integer)newValue);
				return;
			case WorkloadPackage.COMPONENTSIM__SIM_LIFECYCLE_TIME_MAX:
				setSimLifecycleTimeMax((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorkloadPackage.COMPONENTSIM__SIM_LIFECYCLE_TIME_MIN:
				setSimLifecycleTimeMin(SIM_LIFECYCLE_TIME_MIN_EDEFAULT);
				return;
			case WorkloadPackage.COMPONENTSIM__SIM_LIFECYCLE_TIME_MAX:
				setSimLifecycleTimeMax(SIM_LIFECYCLE_TIME_MAX_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorkloadPackage.COMPONENTSIM__SIM_LIFECYCLE_TIME_MIN:
				return SIM_LIFECYCLE_TIME_MIN_EDEFAULT == null ? simLifecycleTimeMin != null : !SIM_LIFECYCLE_TIME_MIN_EDEFAULT.equals(simLifecycleTimeMin);
			case WorkloadPackage.COMPONENTSIM__SIM_LIFECYCLE_TIME_MAX:
				return SIM_LIFECYCLE_TIME_MAX_EDEFAULT == null ? simLifecycleTimeMax != null : !SIM_LIFECYCLE_TIME_MAX_EDEFAULT.equals(simLifecycleTimeMax);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (simLifecycleTimeMin: ");
		result.append(simLifecycleTimeMin);
		result.append(", simLifecycleTimeMax: ");
		result.append(simLifecycleTimeMax);
		result.append(')');
		return result.toString();
	}

} //ComponentsimImpl

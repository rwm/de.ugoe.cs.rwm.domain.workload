/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package de.ugoe.cs.rwm.domain.workload.impl;

import de.ugoe.cs.rwm.domain.workload.Loadsimulator;
import de.ugoe.cs.rwm.domain.workload.WorkloadPackage;

import modmacao.impl.ComponentImpl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Loadsimulator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.impl.LoadsimulatorImpl#getLoadSimulatorTimeout <em>Load Simulator Timeout</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.impl.LoadsimulatorImpl#getLoadSimulatorCpu <em>Load Simulator Cpu</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.impl.LoadsimulatorImpl#getLoadSimulatorIo <em>Load Simulator Io</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.impl.LoadsimulatorImpl#getLoadSimulatorVm <em>Load Simulator Vm</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.impl.LoadsimulatorImpl#getLoadSimulatorVmBytes <em>Load Simulator Vm Bytes</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.impl.LoadsimulatorImpl#getLoadSimulatorVmStride <em>Load Simulator Vm Stride</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.impl.LoadsimulatorImpl#getLoadSimulatorVmHang <em>Load Simulator Vm Hang</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.impl.LoadsimulatorImpl#getLoadSimulatorVmKeep <em>Load Simulator Vm Keep</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.impl.LoadsimulatorImpl#getLoadSimulatorHdd <em>Load Simulator Hdd</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.impl.LoadsimulatorImpl#getLoadSimulatorHddBytes <em>Load Simulator Hdd Bytes</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LoadsimulatorImpl extends ComponentImpl implements Loadsimulator {
	/**
	 * The default value of the '{@link #getLoadSimulatorTimeout() <em>Load Simulator Timeout</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoadSimulatorTimeout()
	 * @generated
	 * @ordered
	 */
	protected static final String LOAD_SIMULATOR_TIMEOUT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLoadSimulatorTimeout() <em>Load Simulator Timeout</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoadSimulatorTimeout()
	 * @generated
	 * @ordered
	 */
	protected String loadSimulatorTimeout = LOAD_SIMULATOR_TIMEOUT_EDEFAULT;

	/**
	 * The default value of the '{@link #getLoadSimulatorCpu() <em>Load Simulator Cpu</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoadSimulatorCpu()
	 * @generated
	 * @ordered
	 */
	protected static final Integer LOAD_SIMULATOR_CPU_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLoadSimulatorCpu() <em>Load Simulator Cpu</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoadSimulatorCpu()
	 * @generated
	 * @ordered
	 */
	protected Integer loadSimulatorCpu = LOAD_SIMULATOR_CPU_EDEFAULT;

	/**
	 * The default value of the '{@link #getLoadSimulatorIo() <em>Load Simulator Io</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoadSimulatorIo()
	 * @generated
	 * @ordered
	 */
	protected static final Integer LOAD_SIMULATOR_IO_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLoadSimulatorIo() <em>Load Simulator Io</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoadSimulatorIo()
	 * @generated
	 * @ordered
	 */
	protected Integer loadSimulatorIo = LOAD_SIMULATOR_IO_EDEFAULT;

	/**
	 * The default value of the '{@link #getLoadSimulatorVm() <em>Load Simulator Vm</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoadSimulatorVm()
	 * @generated
	 * @ordered
	 */
	protected static final Integer LOAD_SIMULATOR_VM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLoadSimulatorVm() <em>Load Simulator Vm</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoadSimulatorVm()
	 * @generated
	 * @ordered
	 */
	protected Integer loadSimulatorVm = LOAD_SIMULATOR_VM_EDEFAULT;

	/**
	 * The default value of the '{@link #getLoadSimulatorVmBytes() <em>Load Simulator Vm Bytes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoadSimulatorVmBytes()
	 * @generated
	 * @ordered
	 */
	protected static final String LOAD_SIMULATOR_VM_BYTES_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLoadSimulatorVmBytes() <em>Load Simulator Vm Bytes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoadSimulatorVmBytes()
	 * @generated
	 * @ordered
	 */
	protected String loadSimulatorVmBytes = LOAD_SIMULATOR_VM_BYTES_EDEFAULT;

	/**
	 * The default value of the '{@link #getLoadSimulatorVmStride() <em>Load Simulator Vm Stride</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoadSimulatorVmStride()
	 * @generated
	 * @ordered
	 */
	protected static final String LOAD_SIMULATOR_VM_STRIDE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLoadSimulatorVmStride() <em>Load Simulator Vm Stride</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoadSimulatorVmStride()
	 * @generated
	 * @ordered
	 */
	protected String loadSimulatorVmStride = LOAD_SIMULATOR_VM_STRIDE_EDEFAULT;

	/**
	 * The default value of the '{@link #getLoadSimulatorVmHang() <em>Load Simulator Vm Hang</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoadSimulatorVmHang()
	 * @generated
	 * @ordered
	 */
	protected static final String LOAD_SIMULATOR_VM_HANG_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLoadSimulatorVmHang() <em>Load Simulator Vm Hang</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoadSimulatorVmHang()
	 * @generated
	 * @ordered
	 */
	protected String loadSimulatorVmHang = LOAD_SIMULATOR_VM_HANG_EDEFAULT;

	/**
	 * The default value of the '{@link #getLoadSimulatorVmKeep() <em>Load Simulator Vm Keep</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoadSimulatorVmKeep()
	 * @generated
	 * @ordered
	 */
	protected static final String LOAD_SIMULATOR_VM_KEEP_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLoadSimulatorVmKeep() <em>Load Simulator Vm Keep</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoadSimulatorVmKeep()
	 * @generated
	 * @ordered
	 */
	protected String loadSimulatorVmKeep = LOAD_SIMULATOR_VM_KEEP_EDEFAULT;

	/**
	 * The default value of the '{@link #getLoadSimulatorHdd() <em>Load Simulator Hdd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoadSimulatorHdd()
	 * @generated
	 * @ordered
	 */
	protected static final Integer LOAD_SIMULATOR_HDD_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLoadSimulatorHdd() <em>Load Simulator Hdd</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoadSimulatorHdd()
	 * @generated
	 * @ordered
	 */
	protected Integer loadSimulatorHdd = LOAD_SIMULATOR_HDD_EDEFAULT;

	/**
	 * The default value of the '{@link #getLoadSimulatorHddBytes() <em>Load Simulator Hdd Bytes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoadSimulatorHddBytes()
	 * @generated
	 * @ordered
	 */
	protected static final String LOAD_SIMULATOR_HDD_BYTES_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLoadSimulatorHddBytes() <em>Load Simulator Hdd Bytes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoadSimulatorHddBytes()
	 * @generated
	 * @ordered
	 */
	protected String loadSimulatorHddBytes = LOAD_SIMULATOR_HDD_BYTES_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LoadsimulatorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkloadPackage.Literals.LOADSIMULATOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLoadSimulatorTimeout() {
		return loadSimulatorTimeout;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLoadSimulatorTimeout(String newLoadSimulatorTimeout) {
		String oldLoadSimulatorTimeout = loadSimulatorTimeout;
		loadSimulatorTimeout = newLoadSimulatorTimeout;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_TIMEOUT, oldLoadSimulatorTimeout, loadSimulatorTimeout));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Integer getLoadSimulatorCpu() {
		return loadSimulatorCpu;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLoadSimulatorCpu(Integer newLoadSimulatorCpu) {
		Integer oldLoadSimulatorCpu = loadSimulatorCpu;
		loadSimulatorCpu = newLoadSimulatorCpu;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_CPU, oldLoadSimulatorCpu, loadSimulatorCpu));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Integer getLoadSimulatorIo() {
		return loadSimulatorIo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLoadSimulatorIo(Integer newLoadSimulatorIo) {
		Integer oldLoadSimulatorIo = loadSimulatorIo;
		loadSimulatorIo = newLoadSimulatorIo;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_IO, oldLoadSimulatorIo, loadSimulatorIo));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Integer getLoadSimulatorVm() {
		return loadSimulatorVm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLoadSimulatorVm(Integer newLoadSimulatorVm) {
		Integer oldLoadSimulatorVm = loadSimulatorVm;
		loadSimulatorVm = newLoadSimulatorVm;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_VM, oldLoadSimulatorVm, loadSimulatorVm));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLoadSimulatorVmBytes() {
		return loadSimulatorVmBytes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLoadSimulatorVmBytes(String newLoadSimulatorVmBytes) {
		String oldLoadSimulatorVmBytes = loadSimulatorVmBytes;
		loadSimulatorVmBytes = newLoadSimulatorVmBytes;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_VM_BYTES, oldLoadSimulatorVmBytes, loadSimulatorVmBytes));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLoadSimulatorVmStride() {
		return loadSimulatorVmStride;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLoadSimulatorVmStride(String newLoadSimulatorVmStride) {
		String oldLoadSimulatorVmStride = loadSimulatorVmStride;
		loadSimulatorVmStride = newLoadSimulatorVmStride;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_VM_STRIDE, oldLoadSimulatorVmStride, loadSimulatorVmStride));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLoadSimulatorVmHang() {
		return loadSimulatorVmHang;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLoadSimulatorVmHang(String newLoadSimulatorVmHang) {
		String oldLoadSimulatorVmHang = loadSimulatorVmHang;
		loadSimulatorVmHang = newLoadSimulatorVmHang;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_VM_HANG, oldLoadSimulatorVmHang, loadSimulatorVmHang));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLoadSimulatorVmKeep() {
		return loadSimulatorVmKeep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLoadSimulatorVmKeep(String newLoadSimulatorVmKeep) {
		String oldLoadSimulatorVmKeep = loadSimulatorVmKeep;
		loadSimulatorVmKeep = newLoadSimulatorVmKeep;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_VM_KEEP, oldLoadSimulatorVmKeep, loadSimulatorVmKeep));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Integer getLoadSimulatorHdd() {
		return loadSimulatorHdd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLoadSimulatorHdd(Integer newLoadSimulatorHdd) {
		Integer oldLoadSimulatorHdd = loadSimulatorHdd;
		loadSimulatorHdd = newLoadSimulatorHdd;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_HDD, oldLoadSimulatorHdd, loadSimulatorHdd));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLoadSimulatorHddBytes() {
		return loadSimulatorHddBytes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLoadSimulatorHddBytes(String newLoadSimulatorHddBytes) {
		String oldLoadSimulatorHddBytes = loadSimulatorHddBytes;
		loadSimulatorHddBytes = newLoadSimulatorHddBytes;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_HDD_BYTES, oldLoadSimulatorHddBytes, loadSimulatorHddBytes));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_TIMEOUT:
				return getLoadSimulatorTimeout();
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_CPU:
				return getLoadSimulatorCpu();
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_IO:
				return getLoadSimulatorIo();
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_VM:
				return getLoadSimulatorVm();
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_VM_BYTES:
				return getLoadSimulatorVmBytes();
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_VM_STRIDE:
				return getLoadSimulatorVmStride();
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_VM_HANG:
				return getLoadSimulatorVmHang();
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_VM_KEEP:
				return getLoadSimulatorVmKeep();
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_HDD:
				return getLoadSimulatorHdd();
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_HDD_BYTES:
				return getLoadSimulatorHddBytes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_TIMEOUT:
				setLoadSimulatorTimeout((String)newValue);
				return;
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_CPU:
				setLoadSimulatorCpu((Integer)newValue);
				return;
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_IO:
				setLoadSimulatorIo((Integer)newValue);
				return;
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_VM:
				setLoadSimulatorVm((Integer)newValue);
				return;
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_VM_BYTES:
				setLoadSimulatorVmBytes((String)newValue);
				return;
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_VM_STRIDE:
				setLoadSimulatorVmStride((String)newValue);
				return;
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_VM_HANG:
				setLoadSimulatorVmHang((String)newValue);
				return;
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_VM_KEEP:
				setLoadSimulatorVmKeep((String)newValue);
				return;
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_HDD:
				setLoadSimulatorHdd((Integer)newValue);
				return;
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_HDD_BYTES:
				setLoadSimulatorHddBytes((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_TIMEOUT:
				setLoadSimulatorTimeout(LOAD_SIMULATOR_TIMEOUT_EDEFAULT);
				return;
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_CPU:
				setLoadSimulatorCpu(LOAD_SIMULATOR_CPU_EDEFAULT);
				return;
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_IO:
				setLoadSimulatorIo(LOAD_SIMULATOR_IO_EDEFAULT);
				return;
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_VM:
				setLoadSimulatorVm(LOAD_SIMULATOR_VM_EDEFAULT);
				return;
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_VM_BYTES:
				setLoadSimulatorVmBytes(LOAD_SIMULATOR_VM_BYTES_EDEFAULT);
				return;
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_VM_STRIDE:
				setLoadSimulatorVmStride(LOAD_SIMULATOR_VM_STRIDE_EDEFAULT);
				return;
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_VM_HANG:
				setLoadSimulatorVmHang(LOAD_SIMULATOR_VM_HANG_EDEFAULT);
				return;
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_VM_KEEP:
				setLoadSimulatorVmKeep(LOAD_SIMULATOR_VM_KEEP_EDEFAULT);
				return;
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_HDD:
				setLoadSimulatorHdd(LOAD_SIMULATOR_HDD_EDEFAULT);
				return;
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_HDD_BYTES:
				setLoadSimulatorHddBytes(LOAD_SIMULATOR_HDD_BYTES_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_TIMEOUT:
				return LOAD_SIMULATOR_TIMEOUT_EDEFAULT == null ? loadSimulatorTimeout != null : !LOAD_SIMULATOR_TIMEOUT_EDEFAULT.equals(loadSimulatorTimeout);
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_CPU:
				return LOAD_SIMULATOR_CPU_EDEFAULT == null ? loadSimulatorCpu != null : !LOAD_SIMULATOR_CPU_EDEFAULT.equals(loadSimulatorCpu);
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_IO:
				return LOAD_SIMULATOR_IO_EDEFAULT == null ? loadSimulatorIo != null : !LOAD_SIMULATOR_IO_EDEFAULT.equals(loadSimulatorIo);
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_VM:
				return LOAD_SIMULATOR_VM_EDEFAULT == null ? loadSimulatorVm != null : !LOAD_SIMULATOR_VM_EDEFAULT.equals(loadSimulatorVm);
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_VM_BYTES:
				return LOAD_SIMULATOR_VM_BYTES_EDEFAULT == null ? loadSimulatorVmBytes != null : !LOAD_SIMULATOR_VM_BYTES_EDEFAULT.equals(loadSimulatorVmBytes);
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_VM_STRIDE:
				return LOAD_SIMULATOR_VM_STRIDE_EDEFAULT == null ? loadSimulatorVmStride != null : !LOAD_SIMULATOR_VM_STRIDE_EDEFAULT.equals(loadSimulatorVmStride);
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_VM_HANG:
				return LOAD_SIMULATOR_VM_HANG_EDEFAULT == null ? loadSimulatorVmHang != null : !LOAD_SIMULATOR_VM_HANG_EDEFAULT.equals(loadSimulatorVmHang);
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_VM_KEEP:
				return LOAD_SIMULATOR_VM_KEEP_EDEFAULT == null ? loadSimulatorVmKeep != null : !LOAD_SIMULATOR_VM_KEEP_EDEFAULT.equals(loadSimulatorVmKeep);
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_HDD:
				return LOAD_SIMULATOR_HDD_EDEFAULT == null ? loadSimulatorHdd != null : !LOAD_SIMULATOR_HDD_EDEFAULT.equals(loadSimulatorHdd);
			case WorkloadPackage.LOADSIMULATOR__LOAD_SIMULATOR_HDD_BYTES:
				return LOAD_SIMULATOR_HDD_BYTES_EDEFAULT == null ? loadSimulatorHddBytes != null : !LOAD_SIMULATOR_HDD_BYTES_EDEFAULT.equals(loadSimulatorHddBytes);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (loadSimulatorTimeout: ");
		result.append(loadSimulatorTimeout);
		result.append(", loadSimulatorCpu: ");
		result.append(loadSimulatorCpu);
		result.append(", loadSimulatorIo: ");
		result.append(loadSimulatorIo);
		result.append(", loadSimulatorVm: ");
		result.append(loadSimulatorVm);
		result.append(", loadSimulatorVmBytes: ");
		result.append(loadSimulatorVmBytes);
		result.append(", loadSimulatorVmStride: ");
		result.append(loadSimulatorVmStride);
		result.append(", loadSimulatorVmHang: ");
		result.append(loadSimulatorVmHang);
		result.append(", loadSimulatorVmKeep: ");
		result.append(loadSimulatorVmKeep);
		result.append(", loadSimulatorHdd: ");
		result.append(loadSimulatorHdd);
		result.append(", loadSimulatorHddBytes: ");
		result.append(loadSimulatorHddBytes);
		result.append(')');
		return result.toString();
	}

} //LoadsimulatorImpl

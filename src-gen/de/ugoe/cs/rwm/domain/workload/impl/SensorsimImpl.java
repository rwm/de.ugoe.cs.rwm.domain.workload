/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package de.ugoe.cs.rwm.domain.workload.impl;

import de.ugoe.cs.rwm.domain.workload.Sensorsim;
import de.ugoe.cs.rwm.domain.workload.WorkloadPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sensorsim</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.impl.SensorsimImpl#getSimChangeRate <em>Sim Change Rate</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.impl.SensorsimImpl#getSimMonitoringResults <em>Sim Monitoring Results</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SensorsimImpl extends SimulationImpl implements Sensorsim {
	/**
	 * The default value of the '{@link #getSimChangeRate() <em>Sim Change Rate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimChangeRate()
	 * @generated
	 * @ordered
	 */
	protected static final Integer SIM_CHANGE_RATE_EDEFAULT = new Integer(2000);

	/**
	 * The cached value of the '{@link #getSimChangeRate() <em>Sim Change Rate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimChangeRate()
	 * @generated
	 * @ordered
	 */
	protected Integer simChangeRate = SIM_CHANGE_RATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getSimMonitoringResults() <em>Sim Monitoring Results</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimMonitoringResults()
	 * @generated
	 * @ordered
	 */
	protected static final String SIM_MONITORING_RESULTS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSimMonitoringResults() <em>Sim Monitoring Results</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimMonitoringResults()
	 * @generated
	 * @ordered
	 */
	protected String simMonitoringResults = SIM_MONITORING_RESULTS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SensorsimImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkloadPackage.Literals.SENSORSIM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Integer getSimChangeRate() {
		return simChangeRate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSimChangeRate(Integer newSimChangeRate) {
		Integer oldSimChangeRate = simChangeRate;
		simChangeRate = newSimChangeRate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkloadPackage.SENSORSIM__SIM_CHANGE_RATE, oldSimChangeRate, simChangeRate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSimMonitoringResults() {
		return simMonitoringResults;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSimMonitoringResults(String newSimMonitoringResults) {
		String oldSimMonitoringResults = simMonitoringResults;
		simMonitoringResults = newSimMonitoringResults;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkloadPackage.SENSORSIM__SIM_MONITORING_RESULTS, oldSimMonitoringResults, simMonitoringResults));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorkloadPackage.SENSORSIM__SIM_CHANGE_RATE:
				return getSimChangeRate();
			case WorkloadPackage.SENSORSIM__SIM_MONITORING_RESULTS:
				return getSimMonitoringResults();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorkloadPackage.SENSORSIM__SIM_CHANGE_RATE:
				setSimChangeRate((Integer)newValue);
				return;
			case WorkloadPackage.SENSORSIM__SIM_MONITORING_RESULTS:
				setSimMonitoringResults((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorkloadPackage.SENSORSIM__SIM_CHANGE_RATE:
				setSimChangeRate(SIM_CHANGE_RATE_EDEFAULT);
				return;
			case WorkloadPackage.SENSORSIM__SIM_MONITORING_RESULTS:
				setSimMonitoringResults(SIM_MONITORING_RESULTS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorkloadPackage.SENSORSIM__SIM_CHANGE_RATE:
				return SIM_CHANGE_RATE_EDEFAULT == null ? simChangeRate != null : !SIM_CHANGE_RATE_EDEFAULT.equals(simChangeRate);
			case WorkloadPackage.SENSORSIM__SIM_MONITORING_RESULTS:
				return SIM_MONITORING_RESULTS_EDEFAULT == null ? simMonitoringResults != null : !SIM_MONITORING_RESULTS_EDEFAULT.equals(simMonitoringResults);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (simChangeRate: ");
		result.append(simChangeRate);
		result.append(", simMonitoringResults: ");
		result.append(simMonitoringResults);
		result.append(')');
		return result.toString();
	}

} //SensorsimImpl

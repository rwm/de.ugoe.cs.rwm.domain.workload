/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package de.ugoe.cs.rwm.domain.workload.impl;

import de.ugoe.cs.rwm.domain.workload.Computesim;
import de.ugoe.cs.rwm.domain.workload.WorkloadPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Computesim</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.impl.ComputesimImpl#getSimVirtualization <em>Sim Virtualization</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.impl.ComputesimImpl#getSimImage <em>Sim Image</em>}</li>
 *   <li>{@link de.ugoe.cs.rwm.domain.workload.impl.ComputesimImpl#getSimOptions <em>Sim Options</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComputesimImpl extends SimulationImpl implements Computesim {
	/**
	 * The default value of the '{@link #getSimVirtualization() <em>Sim Virtualization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimVirtualization()
	 * @generated
	 * @ordered
	 */
	protected static final String SIM_VIRTUALIZATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSimVirtualization() <em>Sim Virtualization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimVirtualization()
	 * @generated
	 * @ordered
	 */
	protected String simVirtualization = SIM_VIRTUALIZATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getSimImage() <em>Sim Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimImage()
	 * @generated
	 * @ordered
	 */
	protected static final String SIM_IMAGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSimImage() <em>Sim Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimImage()
	 * @generated
	 * @ordered
	 */
	protected String simImage = SIM_IMAGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getSimOptions() <em>Sim Options</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimOptions()
	 * @generated
	 * @ordered
	 */
	protected static final String SIM_OPTIONS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSimOptions() <em>Sim Options</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimOptions()
	 * @generated
	 * @ordered
	 */
	protected String simOptions = SIM_OPTIONS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComputesimImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WorkloadPackage.Literals.COMPUTESIM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSimVirtualization() {
		return simVirtualization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSimVirtualization(String newSimVirtualization) {
		String oldSimVirtualization = simVirtualization;
		simVirtualization = newSimVirtualization;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkloadPackage.COMPUTESIM__SIM_VIRTUALIZATION, oldSimVirtualization, simVirtualization));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSimImage() {
		return simImage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSimImage(String newSimImage) {
		String oldSimImage = simImage;
		simImage = newSimImage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkloadPackage.COMPUTESIM__SIM_IMAGE, oldSimImage, simImage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSimOptions() {
		return simOptions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSimOptions(String newSimOptions) {
		String oldSimOptions = simOptions;
		simOptions = newSimOptions;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WorkloadPackage.COMPUTESIM__SIM_OPTIONS, oldSimOptions, simOptions));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WorkloadPackage.COMPUTESIM__SIM_VIRTUALIZATION:
				return getSimVirtualization();
			case WorkloadPackage.COMPUTESIM__SIM_IMAGE:
				return getSimImage();
			case WorkloadPackage.COMPUTESIM__SIM_OPTIONS:
				return getSimOptions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WorkloadPackage.COMPUTESIM__SIM_VIRTUALIZATION:
				setSimVirtualization((String)newValue);
				return;
			case WorkloadPackage.COMPUTESIM__SIM_IMAGE:
				setSimImage((String)newValue);
				return;
			case WorkloadPackage.COMPUTESIM__SIM_OPTIONS:
				setSimOptions((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WorkloadPackage.COMPUTESIM__SIM_VIRTUALIZATION:
				setSimVirtualization(SIM_VIRTUALIZATION_EDEFAULT);
				return;
			case WorkloadPackage.COMPUTESIM__SIM_IMAGE:
				setSimImage(SIM_IMAGE_EDEFAULT);
				return;
			case WorkloadPackage.COMPUTESIM__SIM_OPTIONS:
				setSimOptions(SIM_OPTIONS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WorkloadPackage.COMPUTESIM__SIM_VIRTUALIZATION:
				return SIM_VIRTUALIZATION_EDEFAULT == null ? simVirtualization != null : !SIM_VIRTUALIZATION_EDEFAULT.equals(simVirtualization);
			case WorkloadPackage.COMPUTESIM__SIM_IMAGE:
				return SIM_IMAGE_EDEFAULT == null ? simImage != null : !SIM_IMAGE_EDEFAULT.equals(simImage);
			case WorkloadPackage.COMPUTESIM__SIM_OPTIONS:
				return SIM_OPTIONS_EDEFAULT == null ? simOptions != null : !SIM_OPTIONS_EDEFAULT.equals(simOptions);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (simVirtualization: ");
		result.append(simVirtualization);
		result.append(", simImage: ");
		result.append(simImage);
		result.append(", simOptions: ");
		result.append(simOptions);
		result.append(')');
		return result.toString();
	}

} //ComputesimImpl

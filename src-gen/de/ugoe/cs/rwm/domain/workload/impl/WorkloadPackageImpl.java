/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package de.ugoe.cs.rwm.domain.workload.impl;

import de.ugoe.cs.rwm.domain.workload.Componentsim;
import de.ugoe.cs.rwm.domain.workload.Computesim;
import de.ugoe.cs.rwm.domain.workload.Loadsimulator;
import de.ugoe.cs.rwm.domain.workload.Sensorsim;
import de.ugoe.cs.rwm.domain.workload.Simulation;
import de.ugoe.cs.rwm.domain.workload.WorkloadFactory;
import de.ugoe.cs.rwm.domain.workload.WorkloadPackage;

import de.ugoe.cs.rwm.domain.workload.util.WorkloadValidator;

import modmacao.ModmacaoPackage;

import org.eclipse.cmf.occi.core.OCCIPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EValidator;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.modmacao.occi.platform.PlatformPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class WorkloadPackageImpl extends EPackageImpl implements WorkloadPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass loadsimulatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simulationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass computesimEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass componentsimEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sensorsimEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType stringEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType intEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.ugoe.cs.rwm.domain.workload.WorkloadPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private WorkloadPackageImpl() {
		super(eNS_URI, WorkloadFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link WorkloadPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static WorkloadPackage init() {
		if (isInited) return (WorkloadPackage)EPackage.Registry.INSTANCE.getEPackage(WorkloadPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredWorkloadPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		WorkloadPackageImpl theWorkloadPackage = registeredWorkloadPackage instanceof WorkloadPackageImpl ? (WorkloadPackageImpl)registeredWorkloadPackage : new WorkloadPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		PlatformPackage.eINSTANCE.eClass();
		ModmacaoPackage.eINSTANCE.eClass();
		OCCIPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theWorkloadPackage.createPackageContents();

		// Initialize created meta-data
		theWorkloadPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theWorkloadPackage,
			 new EValidator.Descriptor() {
				 @Override
				 public EValidator getEValidator() {
					 return WorkloadValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theWorkloadPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(WorkloadPackage.eNS_URI, theWorkloadPackage);
		return theWorkloadPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLoadsimulator() {
		return loadsimulatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLoadsimulator_LoadSimulatorTimeout() {
		return (EAttribute)loadsimulatorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLoadsimulator_LoadSimulatorCpu() {
		return (EAttribute)loadsimulatorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLoadsimulator_LoadSimulatorIo() {
		return (EAttribute)loadsimulatorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLoadsimulator_LoadSimulatorVm() {
		return (EAttribute)loadsimulatorEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLoadsimulator_LoadSimulatorVmBytes() {
		return (EAttribute)loadsimulatorEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLoadsimulator_LoadSimulatorVmStride() {
		return (EAttribute)loadsimulatorEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLoadsimulator_LoadSimulatorVmHang() {
		return (EAttribute)loadsimulatorEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLoadsimulator_LoadSimulatorVmKeep() {
		return (EAttribute)loadsimulatorEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLoadsimulator_LoadSimulatorHdd() {
		return (EAttribute)loadsimulatorEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLoadsimulator_LoadSimulatorHddBytes() {
		return (EAttribute)loadsimulatorEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSimulation() {
		return simulationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSimulation_SimLevel() {
		return (EAttribute)simulationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getSimulation__AppliesConstraint__DiagnosticChain_Map() {
		return simulationEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getComputesim() {
		return computesimEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getComputesim_SimVirtualization() {
		return (EAttribute)computesimEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getComputesim_SimImage() {
		return (EAttribute)computesimEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getComputesim_SimOptions() {
		return (EAttribute)computesimEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getComponentsim() {
		return componentsimEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getComponentsim_SimLifecycleTimeMin() {
		return (EAttribute)componentsimEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getComponentsim_SimLifecycleTimeMax() {
		return (EAttribute)componentsimEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSensorsim() {
		return sensorsimEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSensorsim_SimChangeRate() {
		return (EAttribute)sensorsimEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSensorsim_SimMonitoringResults() {
		return (EAttribute)sensorsimEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getString() {
		return stringEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getInt() {
		return intEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public WorkloadFactory getWorkloadFactory() {
		return (WorkloadFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		loadsimulatorEClass = createEClass(LOADSIMULATOR);
		createEAttribute(loadsimulatorEClass, LOADSIMULATOR__LOAD_SIMULATOR_TIMEOUT);
		createEAttribute(loadsimulatorEClass, LOADSIMULATOR__LOAD_SIMULATOR_CPU);
		createEAttribute(loadsimulatorEClass, LOADSIMULATOR__LOAD_SIMULATOR_IO);
		createEAttribute(loadsimulatorEClass, LOADSIMULATOR__LOAD_SIMULATOR_VM);
		createEAttribute(loadsimulatorEClass, LOADSIMULATOR__LOAD_SIMULATOR_VM_BYTES);
		createEAttribute(loadsimulatorEClass, LOADSIMULATOR__LOAD_SIMULATOR_VM_STRIDE);
		createEAttribute(loadsimulatorEClass, LOADSIMULATOR__LOAD_SIMULATOR_VM_HANG);
		createEAttribute(loadsimulatorEClass, LOADSIMULATOR__LOAD_SIMULATOR_VM_KEEP);
		createEAttribute(loadsimulatorEClass, LOADSIMULATOR__LOAD_SIMULATOR_HDD);
		createEAttribute(loadsimulatorEClass, LOADSIMULATOR__LOAD_SIMULATOR_HDD_BYTES);

		simulationEClass = createEClass(SIMULATION);
		createEAttribute(simulationEClass, SIMULATION__SIM_LEVEL);
		createEOperation(simulationEClass, SIMULATION___APPLIES_CONSTRAINT__DIAGNOSTICCHAIN_MAP);

		computesimEClass = createEClass(COMPUTESIM);
		createEAttribute(computesimEClass, COMPUTESIM__SIM_VIRTUALIZATION);
		createEAttribute(computesimEClass, COMPUTESIM__SIM_IMAGE);
		createEAttribute(computesimEClass, COMPUTESIM__SIM_OPTIONS);

		componentsimEClass = createEClass(COMPONENTSIM);
		createEAttribute(componentsimEClass, COMPONENTSIM__SIM_LIFECYCLE_TIME_MIN);
		createEAttribute(componentsimEClass, COMPONENTSIM__SIM_LIFECYCLE_TIME_MAX);

		sensorsimEClass = createEClass(SENSORSIM);
		createEAttribute(sensorsimEClass, SENSORSIM__SIM_CHANGE_RATE);
		createEAttribute(sensorsimEClass, SENSORSIM__SIM_MONITORING_RESULTS);

		// Create data types
		stringEDataType = createEDataType(STRING);
		intEDataType = createEDataType(INT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ModmacaoPackage theModmacaoPackage = (ModmacaoPackage)EPackage.Registry.INSTANCE.getEPackage(ModmacaoPackage.eNS_URI);
		OCCIPackage theOCCIPackage = (OCCIPackage)EPackage.Registry.INSTANCE.getEPackage(OCCIPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		loadsimulatorEClass.getESuperTypes().add(theModmacaoPackage.getComponent());
		loadsimulatorEClass.getESuperTypes().add(theOCCIPackage.getMixinBase());
		simulationEClass.getESuperTypes().add(theOCCIPackage.getMixinBase());
		computesimEClass.getESuperTypes().add(this.getSimulation());
		computesimEClass.getESuperTypes().add(theOCCIPackage.getMixinBase());
		componentsimEClass.getESuperTypes().add(this.getSimulation());
		componentsimEClass.getESuperTypes().add(theOCCIPackage.getMixinBase());
		sensorsimEClass.getESuperTypes().add(this.getSimulation());
		sensorsimEClass.getESuperTypes().add(theOCCIPackage.getMixinBase());

		// Initialize classes, features, and operations; add parameters
		initEClass(loadsimulatorEClass, Loadsimulator.class, "Loadsimulator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLoadsimulator_LoadSimulatorTimeout(), this.getString(), "loadSimulatorTimeout", null, 0, 1, Loadsimulator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLoadsimulator_LoadSimulatorCpu(), this.getInt(), "loadSimulatorCpu", null, 0, 1, Loadsimulator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLoadsimulator_LoadSimulatorIo(), this.getInt(), "loadSimulatorIo", null, 0, 1, Loadsimulator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLoadsimulator_LoadSimulatorVm(), this.getInt(), "loadSimulatorVm", null, 0, 1, Loadsimulator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLoadsimulator_LoadSimulatorVmBytes(), this.getString(), "loadSimulatorVmBytes", null, 0, 1, Loadsimulator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLoadsimulator_LoadSimulatorVmStride(), this.getString(), "loadSimulatorVmStride", null, 0, 1, Loadsimulator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLoadsimulator_LoadSimulatorVmHang(), this.getString(), "loadSimulatorVmHang", null, 0, 1, Loadsimulator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLoadsimulator_LoadSimulatorVmKeep(), this.getString(), "loadSimulatorVmKeep", null, 0, 1, Loadsimulator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLoadsimulator_LoadSimulatorHdd(), this.getInt(), "loadSimulatorHdd", null, 0, 1, Loadsimulator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLoadsimulator_LoadSimulatorHddBytes(), this.getString(), "loadSimulatorHddBytes", null, 0, 1, Loadsimulator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(simulationEClass, Simulation.class, "Simulation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSimulation_SimLevel(), this.getInt(), "simLevel", "0", 1, 1, Simulation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getSimulation__AppliesConstraint__DiagnosticChain_Map(), ecorePackage.getEBoolean(), "appliesConstraint", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		EGenericType g1 = createEGenericType(ecorePackage.getEMap());
		EGenericType g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(computesimEClass, Computesim.class, "Computesim", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getComputesim_SimVirtualization(), this.getString(), "simVirtualization", null, 0, 1, Computesim.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComputesim_SimImage(), this.getString(), "simImage", null, 0, 1, Computesim.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComputesim_SimOptions(), this.getString(), "simOptions", null, 0, 1, Computesim.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(componentsimEClass, Componentsim.class, "Componentsim", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getComponentsim_SimLifecycleTimeMin(), this.getInt(), "simLifecycleTimeMin", "1000", 0, 1, Componentsim.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComponentsim_SimLifecycleTimeMax(), this.getInt(), "simLifecycleTimeMax", "3000", 0, 1, Componentsim.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sensorsimEClass, Sensorsim.class, "Sensorsim", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSensorsim_SimChangeRate(), this.getInt(), "simChangeRate", "2000", 0, 1, Sensorsim.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSensorsim_SimMonitoringResults(), this.getString(), "simMonitoringResults", null, 0, 1, Sensorsim.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize data types
		initEDataType(stringEDataType, String.class, "String", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(intEDataType, Integer.class, "Int", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
		// http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot
		createPivotAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";
		addAnnotation
		  (this,
		   source,
		   new String[] {
		   });
		addAnnotation
		  (simulationEClass,
		   source,
		   new String[] {
			   "constraints", "appliesConstraint"
		   });
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createPivotAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot";
		addAnnotation
		  (getSimulation__AppliesConstraint__DiagnosticChain_Map(),
		   source,
		   new String[] {
			   "body", "self.entity.oclIsKindOf(occi::Resource)"
		   });
	}

} //WorkloadPackageImpl

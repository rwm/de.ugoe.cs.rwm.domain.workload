/*******************************************************************************
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 *************************************************************************
 * This code is 100% auto-generated
 * from:
 *   /de.ugoe.cs.rwm.domain.workload/model/workload.ecore
 * using:
 *   /de.ugoe.cs.rwm.domain.workload/model/workload.genmodel
 *   org.eclipse.ocl.examples.codegen.oclinecore.OCLinEcoreTables
 *
 * Do not edit it.
 *******************************************************************************/
package de.ugoe.cs.rwm.domain.workload;

import de.ugoe.cs.rwm.domain.workload.WorkloadPackage;
import de.ugoe.cs.rwm.domain.workload.WorkloadTables;
import modmacao.ModmacaoTables;
import org.eclipse.cmf.occi.core.OCCIPackage;
import org.eclipse.cmf.occi.core.OCCITables;
import org.eclipse.ocl.pivot.ParameterTypes;
import org.eclipse.ocl.pivot.TemplateParameters;
import org.eclipse.ocl.pivot.ids.ClassId;
import org.eclipse.ocl.pivot.ids.DataTypeId;
import org.eclipse.ocl.pivot.ids.IdManager;
import org.eclipse.ocl.pivot.ids.NsURIPackageId;
import org.eclipse.ocl.pivot.ids.RootPackageId;
import org.eclipse.ocl.pivot.ids.TypeId;
import org.eclipse.ocl.pivot.internal.library.ecore.EcoreExecutorPackage;
import org.eclipse.ocl.pivot.internal.library.ecore.EcoreExecutorProperty;
import org.eclipse.ocl.pivot.internal.library.ecore.EcoreExecutorType;
import org.eclipse.ocl.pivot.internal.library.executor.ExecutorFragment;
import org.eclipse.ocl.pivot.internal.library.executor.ExecutorOperation;
import org.eclipse.ocl.pivot.internal.library.executor.ExecutorProperty;
import org.eclipse.ocl.pivot.internal.library.executor.ExecutorStandardLibrary;
import org.eclipse.ocl.pivot.oclstdlib.OCLstdlibTables;
import org.eclipse.ocl.pivot.utilities.TypeUtil;
import org.eclipse.ocl.pivot.utilities.ValueUtil;
import org.eclipse.ocl.pivot.values.IntegerValue;

/**
 * WorkloadTables provides the dispatch tables for the workload for use by the OCL dispatcher.
 *
 * In order to ensure correct static initialization, a top level class element must be accessed
 * before any nested class element. Therefore an access to PACKAGE.getClass() is recommended.
 */
public class WorkloadTables
{
	static {
		Init.initStart();
	}

	/**
	 *	The package descriptor for the package.
	 */
	public static final EcoreExecutorPackage PACKAGE = new EcoreExecutorPackage(WorkloadPackage.eINSTANCE);

	/**
	 *	The library of all packages and types.
	 */
	public static final ExecutorStandardLibrary LIBRARY = OCLstdlibTables.LIBRARY;

	/**
	 *	Constants used by auto-generated code.
	 */
	public static final /*@NonInvalid*/ RootPackageId PACKid_$metamodel$ = IdManager.getRootPackageId("$metamodel$");
	public static final /*@NonInvalid*/ NsURIPackageId PACKid_http_c_s_s_schemas_ogf_org_s_occi_s_core_s_ecore = IdManager.getNsURIPackageId("http://schemas.ogf.org/occi/core/ecore", "occi", OCCIPackage.eINSTANCE);
	public static final /*@NonInvalid*/ NsURIPackageId PACKid_http_c_s_s_schemas_ugoe_cs_rwm_s_domain_s_workload_s_ecore = IdManager.getNsURIPackageId("http://schemas.ugoe.cs.rwm/domain/workload/ecore", null, WorkloadPackage.eINSTANCE);
	public static final /*@NonInvalid*/ ClassId CLSSid_Class = WorkloadTables.PACKid_$metamodel$.getClassId("Class", 0);
	public static final /*@NonInvalid*/ ClassId CLSSid_Entity = WorkloadTables.PACKid_http_c_s_s_schemas_ogf_org_s_occi_s_core_s_ecore.getClassId("Entity", 0);
	public static final /*@NonInvalid*/ ClassId CLSSid_Resource = WorkloadTables.PACKid_http_c_s_s_schemas_ogf_org_s_occi_s_core_s_ecore.getClassId("Resource", 0);
	public static final /*@NonInvalid*/ ClassId CLSSid_Simulation = WorkloadTables.PACKid_http_c_s_s_schemas_ugoe_cs_rwm_s_domain_s_workload_s_ecore.getClassId("Simulation", 0);
	public static final /*@NonInvalid*/ DataTypeId DATAid_Int = WorkloadTables.PACKid_http_c_s_s_schemas_ugoe_cs_rwm_s_domain_s_workload_s_ecore.getDataTypeId("Int", 0);
	public static final /*@NonInvalid*/ IntegerValue INT_0 = ValueUtil.integerValueOf("0");

	/**
	 *	The type parameters for templated types and operations.
	 */
	public static class TypeParameters {
		static {
			Init.initStart();
			WorkloadTables.init();
		}

		static {
			Init.initEnd();
		}

		/**
		 * Force initialization of the fields of WorkloadTables::TypeParameters and all preceding sub-packages.
		 */
		public static void init() {}
	}

	/**
	 *	The type descriptors for each type.
	 */
	public static class Types {
		static {
			Init.initStart();
			TypeParameters.init();
		}

		public static final EcoreExecutorType _Componentsim = new EcoreExecutorType(WorkloadPackage.Literals.COMPONENTSIM, PACKAGE, 0);
		public static final EcoreExecutorType _Computesim = new EcoreExecutorType(WorkloadPackage.Literals.COMPUTESIM, PACKAGE, 0);
		public static final EcoreExecutorType _Int = new EcoreExecutorType("Int", PACKAGE, 0);
		public static final EcoreExecutorType _Loadsimulator = new EcoreExecutorType(WorkloadPackage.Literals.LOADSIMULATOR, PACKAGE, 0);
		public static final EcoreExecutorType _Sensorsim = new EcoreExecutorType(WorkloadPackage.Literals.SENSORSIM, PACKAGE, 0);
		public static final EcoreExecutorType _Simulation = new EcoreExecutorType(WorkloadPackage.Literals.SIMULATION, PACKAGE, 0);
		public static final EcoreExecutorType _String = new EcoreExecutorType(TypeId.STRING, PACKAGE, 0);

		private static final EcoreExecutorType /*@NonNull*/ [] types = {
			_Componentsim,
			_Computesim,
			_Int,
			_Loadsimulator,
			_Sensorsim,
			_Simulation,
			_String
		};

		/*
		 *	Install the type descriptors in the package descriptor.
		 */
		static {
			PACKAGE.init(LIBRARY, types);
			Init.initEnd();
		}

		/**
		 * Force initialization of the fields of WorkloadTables::Types and all preceding sub-packages.
		 */
		public static void init() {}
	}

	/**
	 *	The fragment descriptors for the local elements of each type and its supertypes.
	 */
	public static class Fragments {
		static {
			Init.initStart();
			Types.init();
		}

		private static final ExecutorFragment _Componentsim__Componentsim = new ExecutorFragment(Types._Componentsim, WorkloadTables.Types._Componentsim);
		private static final ExecutorFragment _Componentsim__MixinBase = new ExecutorFragment(Types._Componentsim, OCCITables.Types._MixinBase);
		private static final ExecutorFragment _Componentsim__OclAny = new ExecutorFragment(Types._Componentsim, OCLstdlibTables.Types._OclAny);
		private static final ExecutorFragment _Componentsim__OclElement = new ExecutorFragment(Types._Componentsim, OCLstdlibTables.Types._OclElement);
		private static final ExecutorFragment _Componentsim__Simulation = new ExecutorFragment(Types._Componentsim, WorkloadTables.Types._Simulation);

		private static final ExecutorFragment _Computesim__Computesim = new ExecutorFragment(Types._Computesim, WorkloadTables.Types._Computesim);
		private static final ExecutorFragment _Computesim__MixinBase = new ExecutorFragment(Types._Computesim, OCCITables.Types._MixinBase);
		private static final ExecutorFragment _Computesim__OclAny = new ExecutorFragment(Types._Computesim, OCLstdlibTables.Types._OclAny);
		private static final ExecutorFragment _Computesim__OclElement = new ExecutorFragment(Types._Computesim, OCLstdlibTables.Types._OclElement);
		private static final ExecutorFragment _Computesim__Simulation = new ExecutorFragment(Types._Computesim, WorkloadTables.Types._Simulation);

		private static final ExecutorFragment _Int__Int = new ExecutorFragment(Types._Int, WorkloadTables.Types._Int);
		private static final ExecutorFragment _Int__OclAny = new ExecutorFragment(Types._Int, OCLstdlibTables.Types._OclAny);

		private static final ExecutorFragment _Loadsimulator__Component = new ExecutorFragment(Types._Loadsimulator, ModmacaoTables.Types._Component);
		private static final ExecutorFragment _Loadsimulator__Loadsimulator = new ExecutorFragment(Types._Loadsimulator, WorkloadTables.Types._Loadsimulator);
		private static final ExecutorFragment _Loadsimulator__MixinBase = new ExecutorFragment(Types._Loadsimulator, OCCITables.Types._MixinBase);
		private static final ExecutorFragment _Loadsimulator__OclAny = new ExecutorFragment(Types._Loadsimulator, OCLstdlibTables.Types._OclAny);
		private static final ExecutorFragment _Loadsimulator__OclElement = new ExecutorFragment(Types._Loadsimulator, OCLstdlibTables.Types._OclElement);

		private static final ExecutorFragment _Sensorsim__MixinBase = new ExecutorFragment(Types._Sensorsim, OCCITables.Types._MixinBase);
		private static final ExecutorFragment _Sensorsim__OclAny = new ExecutorFragment(Types._Sensorsim, OCLstdlibTables.Types._OclAny);
		private static final ExecutorFragment _Sensorsim__OclElement = new ExecutorFragment(Types._Sensorsim, OCLstdlibTables.Types._OclElement);
		private static final ExecutorFragment _Sensorsim__Sensorsim = new ExecutorFragment(Types._Sensorsim, WorkloadTables.Types._Sensorsim);
		private static final ExecutorFragment _Sensorsim__Simulation = new ExecutorFragment(Types._Sensorsim, WorkloadTables.Types._Simulation);

		private static final ExecutorFragment _Simulation__MixinBase = new ExecutorFragment(Types._Simulation, OCCITables.Types._MixinBase);
		private static final ExecutorFragment _Simulation__OclAny = new ExecutorFragment(Types._Simulation, OCLstdlibTables.Types._OclAny);
		private static final ExecutorFragment _Simulation__OclElement = new ExecutorFragment(Types._Simulation, OCLstdlibTables.Types._OclElement);
		private static final ExecutorFragment _Simulation__Simulation = new ExecutorFragment(Types._Simulation, WorkloadTables.Types._Simulation);

		private static final ExecutorFragment _String__OclAny = new ExecutorFragment(Types._String, OCLstdlibTables.Types._OclAny);
		private static final ExecutorFragment _String__OclComparable = new ExecutorFragment(Types._String, OCLstdlibTables.Types._OclComparable);
		private static final ExecutorFragment _String__OclSummable = new ExecutorFragment(Types._String, OCLstdlibTables.Types._OclSummable);
		private static final ExecutorFragment _String__String = new ExecutorFragment(Types._String, WorkloadTables.Types._String);

		static {
			Init.initEnd();
		}

		/**
		 * Force initialization of the fields of WorkloadTables::Fragments and all preceding sub-packages.
		 */
		public static void init() {}
	}

	/**
	 *	The parameter lists shared by operations.
	 *
	 * @noextend This class is not intended to be subclassed by clients.
	 * @noinstantiate This class is not intended to be instantiated by clients.
	 * @noreference This class is not intended to be referenced by clients.
	 */
	public static class Parameters {
		static {
			Init.initStart();
			Fragments.init();
		}

		public static final ParameterTypes _Integer = TypeUtil.createParameterTypes(OCLstdlibTables.Types._Integer);
		public static final ParameterTypes _Integer___Integer = TypeUtil.createParameterTypes(OCLstdlibTables.Types._Integer, OCLstdlibTables.Types._Integer);
		public static final ParameterTypes _OclAny___OclAny___OclAny___Integer___Boolean___Integer = TypeUtil.createParameterTypes(OCLstdlibTables.Types._OclAny, OCLstdlibTables.Types._OclAny, OCLstdlibTables.Types._OclAny, OCLstdlibTables.Types._Integer, OCLstdlibTables.Types._Boolean, OCLstdlibTables.Types._Integer);
		public static final ParameterTypes _OclAny___OclAny___OclAny___OclAny___String___Integer___OclAny___Integer = TypeUtil.createParameterTypes(OCLstdlibTables.Types._OclAny, OCLstdlibTables.Types._OclAny, OCLstdlibTables.Types._OclAny, OCLstdlibTables.Types._OclAny, OCLstdlibTables.Types._String, OCLstdlibTables.Types._Integer, OCLstdlibTables.Types._OclAny, OCLstdlibTables.Types._Integer);
		public static final ParameterTypes _OclSelf = TypeUtil.createParameterTypes(OCLstdlibTables.Types._OclSelf);
		public static final ParameterTypes _String = TypeUtil.createParameterTypes(OCLstdlibTables.Types._String);
		public static final ParameterTypes _String___Boolean = TypeUtil.createParameterTypes(OCLstdlibTables.Types._String, OCLstdlibTables.Types._Boolean);
		public static final ParameterTypes _String___String = TypeUtil.createParameterTypes(OCLstdlibTables.Types._String, OCLstdlibTables.Types._String);

		static {
			Init.initEnd();
		}

		/**
		 * Force initialization of the fields of WorkloadTables::Parameters and all preceding sub-packages.
		 */
		public static void init() {}
	}

	/**
	 *	The operation descriptors for each operation of each type.
	 *
	 * @noextend This class is not intended to be subclassed by clients.
	 * @noinstantiate This class is not intended to be instantiated by clients.
	 * @noreference This class is not intended to be referenced by clients.
	 */
	public static class Operations {
		static {
			Init.initStart();
			Parameters.init();
		}

		public static final ExecutorOperation _String___add_ = new ExecutorOperation("+", Parameters._String, Types._String,
			0, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringConcatOperation.INSTANCE);
		public static final ExecutorOperation _String___lt_ = new ExecutorOperation("<", Parameters._OclSelf, Types._String,
			1, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringLessThanOperation.INSTANCE);
		public static final ExecutorOperation _String___lt__eq_ = new ExecutorOperation("<=", Parameters._OclSelf, Types._String,
			2, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringLessThanEqualOperation.INSTANCE);
		public static final ExecutorOperation _String___lt__gt_ = new ExecutorOperation("<>", Parameters._OclSelf, Types._String,
			3, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.oclany.OclAnyNotEqualOperation.INSTANCE);
		public static final ExecutorOperation _String___eq_ = new ExecutorOperation("=", Parameters._OclSelf, Types._String,
			4, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.oclany.OclAnyEqualOperation.INSTANCE);
		public static final ExecutorOperation _String___gt_ = new ExecutorOperation(">", Parameters._OclSelf, Types._String,
			5, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringGreaterThanOperation.INSTANCE);
		public static final ExecutorOperation _String___gt__eq_ = new ExecutorOperation(">=", Parameters._OclSelf, Types._String,
			6, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringGreaterThanEqualOperation.INSTANCE);
		public static final ExecutorOperation _String__at = new ExecutorOperation("at", Parameters._Integer, Types._String,
			7, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringAtOperation.INSTANCE);
		public static final ExecutorOperation _String__characters = new ExecutorOperation("characters", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			8, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringCharactersOperation.INSTANCE);
		public static final ExecutorOperation _String__compareTo = new ExecutorOperation("compareTo", Parameters._OclSelf, Types._String,
			9, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringCompareToOperation.INSTANCE);
		public static final ExecutorOperation _String__concat = new ExecutorOperation("concat", Parameters._String, Types._String,
			10, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringConcatOperation.INSTANCE);
		public static final ExecutorOperation _String__endsWith = new ExecutorOperation("endsWith", Parameters._String, Types._String,
			11, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringEndsWithOperation.INSTANCE);
		public static final ExecutorOperation _String__equalsIgnoreCase = new ExecutorOperation("equalsIgnoreCase", Parameters._String, Types._String,
			12, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringEqualsIgnoreCaseOperation.INSTANCE);
		public static final ExecutorOperation _String__getSeverity = new ExecutorOperation("getSeverity", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			13, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.CGStringGetSeverityOperation.INSTANCE);
		public static final ExecutorOperation _String__indexOf = new ExecutorOperation("indexOf", Parameters._String, Types._String,
			14, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringIndexOfOperation.INSTANCE);
		public static final ExecutorOperation _String__lastIndexOf = new ExecutorOperation("lastIndexOf", Parameters._String, Types._String,
			15, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringLastIndexOfOperation.INSTANCE);
		public static final ExecutorOperation _String__0_logDiagnostic = new ExecutorOperation("logDiagnostic", Parameters._OclAny___OclAny___OclAny___Integer___Boolean___Integer, Types._String,
			16, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.CGStringLogDiagnosticOperation.INSTANCE);
		public static final ExecutorOperation _String__1_logDiagnostic = new ExecutorOperation("logDiagnostic", Parameters._OclAny___OclAny___OclAny___OclAny___String___Integer___OclAny___Integer, Types._String,
			17, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.CGStringLogDiagnosticOperation.INSTANCE);
		public static final ExecutorOperation _String__matches = new ExecutorOperation("matches", Parameters._String, Types._String,
			18, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringMatchesOperation.INSTANCE);
		public static final ExecutorOperation _String__replaceAll = new ExecutorOperation("replaceAll", Parameters._String___String, Types._String,
			19, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringReplaceAllOperation.INSTANCE);
		public static final ExecutorOperation _String__replaceFirst = new ExecutorOperation("replaceFirst", Parameters._String___String, Types._String,
			20, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringReplaceFirstOperation.INSTANCE);
		public static final ExecutorOperation _String__size = new ExecutorOperation("size", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			21, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringSizeOperation.INSTANCE);
		public static final ExecutorOperation _String__startsWith = new ExecutorOperation("startsWith", Parameters._String, Types._String,
			22, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringStartsWithOperation.INSTANCE);
		public static final ExecutorOperation _String__substituteAll = new ExecutorOperation("substituteAll", Parameters._String___String, Types._String,
			23, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringSubstituteAllOperation.INSTANCE);
		public static final ExecutorOperation _String__substituteFirst = new ExecutorOperation("substituteFirst", Parameters._String___String, Types._String,
			24, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringSubstituteFirstOperation.INSTANCE);
		public static final ExecutorOperation _String__substring = new ExecutorOperation("substring", Parameters._Integer___Integer, Types._String,
			25, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringSubstringOperation.INSTANCE);
		public static final ExecutorOperation _String__toBoolean = new ExecutorOperation("toBoolean", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			26, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringToBooleanOperation.INSTANCE);
		public static final ExecutorOperation _String__toInteger = new ExecutorOperation("toInteger", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			27, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringToIntegerOperation.INSTANCE);
		public static final ExecutorOperation _String__toLower = new ExecutorOperation("toLower", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			28, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringToLowerCaseOperation.INSTANCE);
		public static final ExecutorOperation _String__toLowerCase = new ExecutorOperation("toLowerCase", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			29, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringToLowerCaseOperation.INSTANCE);
		public static final ExecutorOperation _String__toReal = new ExecutorOperation("toReal", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			30, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringToRealOperation.INSTANCE);
		public static final ExecutorOperation _String__toString = new ExecutorOperation("toString", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			31, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.oclany.OclAnyToStringOperation.INSTANCE);
		public static final ExecutorOperation _String__toUpper = new ExecutorOperation("toUpper", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			32, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringToUpperCaseOperation.INSTANCE);
		public static final ExecutorOperation _String__toUpperCase = new ExecutorOperation("toUpperCase", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			33, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringToUpperCaseOperation.INSTANCE);
		public static final ExecutorOperation _String__0_tokenize = new ExecutorOperation("tokenize", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			34, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringTokenizeOperation.INSTANCE);
		public static final ExecutorOperation _String__2_tokenize = new ExecutorOperation("tokenize", Parameters._String___Boolean, Types._String,
			35, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringTokenizeOperation.INSTANCE);
		public static final ExecutorOperation _String__1_tokenize = new ExecutorOperation("tokenize", Parameters._String, Types._String,
			36, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringTokenizeOperation.INSTANCE);
		public static final ExecutorOperation _String__trim = new ExecutorOperation("trim", TypeUtil.EMPTY_PARAMETER_TYPES, Types._String,
			37, TemplateParameters.EMPTY_LIST, org.eclipse.ocl.pivot.library.string.StringTrimOperation.INSTANCE);

		static {
			Init.initEnd();
		}

		/**
		 * Force initialization of the fields of WorkloadTables::Operations and all preceding sub-packages.
		 */
		public static void init() {}
	}

	/**
	 *	The property descriptors for each property of each type.
	 *
	 * @noextend This class is not intended to be subclassed by clients.
	 * @noinstantiate This class is not intended to be instantiated by clients.
	 * @noreference This class is not intended to be referenced by clients.
	 */
	public static class Properties {
		static {
			Init.initStart();
			Operations.init();
		}

		public static final ExecutorProperty _Componentsim__simLifecycleTimeMax = new EcoreExecutorProperty(WorkloadPackage.Literals.COMPONENTSIM__SIM_LIFECYCLE_TIME_MAX, Types._Componentsim, 0);
		public static final ExecutorProperty _Componentsim__simLifecycleTimeMin = new EcoreExecutorProperty(WorkloadPackage.Literals.COMPONENTSIM__SIM_LIFECYCLE_TIME_MIN, Types._Componentsim, 1);

		public static final ExecutorProperty _Computesim__simImage = new EcoreExecutorProperty(WorkloadPackage.Literals.COMPUTESIM__SIM_IMAGE, Types._Computesim, 0);
		public static final ExecutorProperty _Computesim__simOptions = new EcoreExecutorProperty(WorkloadPackage.Literals.COMPUTESIM__SIM_OPTIONS, Types._Computesim, 1);
		public static final ExecutorProperty _Computesim__simVirtualization = new EcoreExecutorProperty(WorkloadPackage.Literals.COMPUTESIM__SIM_VIRTUALIZATION, Types._Computesim, 2);

		public static final ExecutorProperty _Loadsimulator__loadSimulatorCpu = new EcoreExecutorProperty(WorkloadPackage.Literals.LOADSIMULATOR__LOAD_SIMULATOR_CPU, Types._Loadsimulator, 0);
		public static final ExecutorProperty _Loadsimulator__loadSimulatorHdd = new EcoreExecutorProperty(WorkloadPackage.Literals.LOADSIMULATOR__LOAD_SIMULATOR_HDD, Types._Loadsimulator, 1);
		public static final ExecutorProperty _Loadsimulator__loadSimulatorHddBytes = new EcoreExecutorProperty(WorkloadPackage.Literals.LOADSIMULATOR__LOAD_SIMULATOR_HDD_BYTES, Types._Loadsimulator, 2);
		public static final ExecutorProperty _Loadsimulator__loadSimulatorIo = new EcoreExecutorProperty(WorkloadPackage.Literals.LOADSIMULATOR__LOAD_SIMULATOR_IO, Types._Loadsimulator, 3);
		public static final ExecutorProperty _Loadsimulator__loadSimulatorTimeout = new EcoreExecutorProperty(WorkloadPackage.Literals.LOADSIMULATOR__LOAD_SIMULATOR_TIMEOUT, Types._Loadsimulator, 4);
		public static final ExecutorProperty _Loadsimulator__loadSimulatorVm = new EcoreExecutorProperty(WorkloadPackage.Literals.LOADSIMULATOR__LOAD_SIMULATOR_VM, Types._Loadsimulator, 5);
		public static final ExecutorProperty _Loadsimulator__loadSimulatorVmBytes = new EcoreExecutorProperty(WorkloadPackage.Literals.LOADSIMULATOR__LOAD_SIMULATOR_VM_BYTES, Types._Loadsimulator, 6);
		public static final ExecutorProperty _Loadsimulator__loadSimulatorVmHang = new EcoreExecutorProperty(WorkloadPackage.Literals.LOADSIMULATOR__LOAD_SIMULATOR_VM_HANG, Types._Loadsimulator, 7);
		public static final ExecutorProperty _Loadsimulator__loadSimulatorVmKeep = new EcoreExecutorProperty(WorkloadPackage.Literals.LOADSIMULATOR__LOAD_SIMULATOR_VM_KEEP, Types._Loadsimulator, 8);
		public static final ExecutorProperty _Loadsimulator__loadSimulatorVmStride = new EcoreExecutorProperty(WorkloadPackage.Literals.LOADSIMULATOR__LOAD_SIMULATOR_VM_STRIDE, Types._Loadsimulator, 9);

		public static final ExecutorProperty _Sensorsim__simChangeRate = new EcoreExecutorProperty(WorkloadPackage.Literals.SENSORSIM__SIM_CHANGE_RATE, Types._Sensorsim, 0);
		public static final ExecutorProperty _Sensorsim__simMonitoringResults = new EcoreExecutorProperty(WorkloadPackage.Literals.SENSORSIM__SIM_MONITORING_RESULTS, Types._Sensorsim, 1);

		public static final ExecutorProperty _Simulation__simLevel = new EcoreExecutorProperty(WorkloadPackage.Literals.SIMULATION__SIM_LEVEL, Types._Simulation, 0);
		static {
			Init.initEnd();
		}

		/**
		 * Force initialization of the fields of WorkloadTables::Properties and all preceding sub-packages.
		 */
		public static void init() {}
	}

	/**
	 *	The fragments for all base types in depth order: OclAny first, OclSelf last.
	 */
	public static class TypeFragments {
		static {
			Init.initStart();
			Properties.init();
		}

		private static final ExecutorFragment /*@NonNull*/ [] _Componentsim =
			{
				Fragments._Componentsim__OclAny /* 0 */,
				Fragments._Componentsim__OclElement /* 1 */,
				Fragments._Componentsim__MixinBase /* 2 */,
				Fragments._Componentsim__Simulation /* 3 */,
				Fragments._Componentsim__Componentsim /* 4 */
			};
		private static final int /*@NonNull*/ [] __Componentsim = { 1,1,1,1,1 };

		private static final ExecutorFragment /*@NonNull*/ [] _Computesim =
			{
				Fragments._Computesim__OclAny /* 0 */,
				Fragments._Computesim__OclElement /* 1 */,
				Fragments._Computesim__MixinBase /* 2 */,
				Fragments._Computesim__Simulation /* 3 */,
				Fragments._Computesim__Computesim /* 4 */
			};
		private static final int /*@NonNull*/ [] __Computesim = { 1,1,1,1,1 };

		private static final ExecutorFragment /*@NonNull*/ [] _Int =
			{
				Fragments._Int__OclAny /* 0 */,
				Fragments._Int__Int /* 1 */
			};
		private static final int /*@NonNull*/ [] __Int = { 1,1 };

		private static final ExecutorFragment /*@NonNull*/ [] _Loadsimulator =
			{
				Fragments._Loadsimulator__OclAny /* 0 */,
				Fragments._Loadsimulator__OclElement /* 1 */,
				Fragments._Loadsimulator__MixinBase /* 2 */,
				Fragments._Loadsimulator__Component /* 3 */,
				Fragments._Loadsimulator__Loadsimulator /* 4 */
			};
		private static final int /*@NonNull*/ [] __Loadsimulator = { 1,1,1,1,1 };

		private static final ExecutorFragment /*@NonNull*/ [] _Sensorsim =
			{
				Fragments._Sensorsim__OclAny /* 0 */,
				Fragments._Sensorsim__OclElement /* 1 */,
				Fragments._Sensorsim__MixinBase /* 2 */,
				Fragments._Sensorsim__Simulation /* 3 */,
				Fragments._Sensorsim__Sensorsim /* 4 */
			};
		private static final int /*@NonNull*/ [] __Sensorsim = { 1,1,1,1,1 };

		private static final ExecutorFragment /*@NonNull*/ [] _Simulation =
			{
				Fragments._Simulation__OclAny /* 0 */,
				Fragments._Simulation__OclElement /* 1 */,
				Fragments._Simulation__MixinBase /* 2 */,
				Fragments._Simulation__Simulation /* 3 */
			};
		private static final int /*@NonNull*/ [] __Simulation = { 1,1,1,1 };

		private static final ExecutorFragment /*@NonNull*/ [] _String =
			{
				Fragments._String__OclAny /* 0 */,
				Fragments._String__OclComparable /* 1 */,
				Fragments._String__OclSummable /* 1 */,
				Fragments._String__String /* 2 */
			};
		private static final int /*@NonNull*/ [] __String = { 1,2,1 };

		/**
		 *	Install the fragment descriptors in the class descriptors.
		 */
		static {
			Types._Componentsim.initFragments(_Componentsim, __Componentsim);
			Types._Computesim.initFragments(_Computesim, __Computesim);
			Types._Int.initFragments(_Int, __Int);
			Types._Loadsimulator.initFragments(_Loadsimulator, __Loadsimulator);
			Types._Sensorsim.initFragments(_Sensorsim, __Sensorsim);
			Types._Simulation.initFragments(_Simulation, __Simulation);
			Types._String.initFragments(_String, __String);

			Init.initEnd();
		}

		/**
		 * Force initialization of the fields of WorkloadTables::TypeFragments and all preceding sub-packages.
		 */
		public static void init() {}
	}

	/**
	 *	The lists of local operations or local operation overrides for each fragment of each type.
	 */
	public static class FragmentOperations {
		static {
			Init.initStart();
			TypeFragments.init();
		}

		private static final ExecutorOperation /*@NonNull*/ [] _Componentsim__Componentsim = {};
		private static final ExecutorOperation /*@NonNull*/ [] _Componentsim__MixinBase = {};
		private static final ExecutorOperation /*@NonNull*/ [] _Componentsim__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[1]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[1]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final ExecutorOperation /*@NonNull*/ [] _Componentsim__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[1]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};
		private static final ExecutorOperation /*@NonNull*/ [] _Componentsim__Simulation = {};

		private static final ExecutorOperation /*@NonNull*/ [] _Computesim__Computesim = {};
		private static final ExecutorOperation /*@NonNull*/ [] _Computesim__MixinBase = {};
		private static final ExecutorOperation /*@NonNull*/ [] _Computesim__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[1]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[1]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final ExecutorOperation /*@NonNull*/ [] _Computesim__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[1]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};
		private static final ExecutorOperation /*@NonNull*/ [] _Computesim__Simulation = {};

		private static final ExecutorOperation /*@NonNull*/ [] _Int__Int = {};
		private static final ExecutorOperation /*@NonNull*/ [] _Int__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[1]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[1]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};

		private static final ExecutorOperation /*@NonNull*/ [] _Loadsimulator__Loadsimulator = {};
		private static final ExecutorOperation /*@NonNull*/ [] _Loadsimulator__Component = {};
		private static final ExecutorOperation /*@NonNull*/ [] _Loadsimulator__MixinBase = {};
		private static final ExecutorOperation /*@NonNull*/ [] _Loadsimulator__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[1]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[1]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final ExecutorOperation /*@NonNull*/ [] _Loadsimulator__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[1]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};

		private static final ExecutorOperation /*@NonNull*/ [] _Sensorsim__Sensorsim = {};
		private static final ExecutorOperation /*@NonNull*/ [] _Sensorsim__MixinBase = {};
		private static final ExecutorOperation /*@NonNull*/ [] _Sensorsim__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[1]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[1]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final ExecutorOperation /*@NonNull*/ [] _Sensorsim__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[1]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};
		private static final ExecutorOperation /*@NonNull*/ [] _Sensorsim__Simulation = {};

		private static final ExecutorOperation /*@NonNull*/ [] _Simulation__Simulation = {};
		private static final ExecutorOperation /*@NonNull*/ [] _Simulation__MixinBase = {};
		private static final ExecutorOperation /*@NonNull*/ [] _Simulation__OclAny = {
			OCLstdlibTables.Operations._OclAny___lt__gt_ /* _'<>'(OclSelf[1]) */,
			OCLstdlibTables.Operations._OclAny___eq_ /* _'='(OclSelf[1]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._OclAny__toString /* toString() */
		};
		private static final ExecutorOperation /*@NonNull*/ [] _Simulation__OclElement = {
			OCLstdlibTables.Operations._OclElement__allInstances /* allInstances() */,
			OCLstdlibTables.Operations._OclElement__oclAsModelType /* oclAsModelType(TT)(TT[1]) */,
			OCLstdlibTables.Operations._OclElement__oclContainer /* oclContainer() */,
			OCLstdlibTables.Operations._OclElement__oclContents /* oclContents() */,
			OCLstdlibTables.Operations._OclElement__oclIsModelKindOf /* oclIsModelKindOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclElement__oclModelType /* oclModelType() */,
			OCLstdlibTables.Operations._OclElement__oclModelTypes /* oclModelTypes() */
		};

		private static final ExecutorOperation /*@NonNull*/ [] _String__String = {
			OCLstdlibTables.Operations._String___add_ /* _'+'(String[?]) */,
			OCLstdlibTables.Operations._String___lt_ /* _'<'(OclSelf[1]) */,
			OCLstdlibTables.Operations._String___lt__eq_ /* _'<='(OclSelf[1]) */,
			OCLstdlibTables.Operations._String___lt__gt_ /* _'<>'(OclSelf[1]) */,
			OCLstdlibTables.Operations._String___eq_ /* _'='(OclSelf[1]) */,
			OCLstdlibTables.Operations._String___gt_ /* _'>'(OclSelf[1]) */,
			OCLstdlibTables.Operations._String___gt__eq_ /* _'>='(OclSelf[1]) */,
			OCLstdlibTables.Operations._String__at /* at(Integer[1]) */,
			OCLstdlibTables.Operations._String__characters /* characters() */,
			OCLstdlibTables.Operations._String__compareTo /* compareTo(OclSelf[1]) */,
			OCLstdlibTables.Operations._String__concat /* concat(String[?]) */,
			OCLstdlibTables.Operations._String__endsWith /* endsWith(String[1]) */,
			OCLstdlibTables.Operations._String__equalsIgnoreCase /* equalsIgnoreCase(String[1]) */,
			//OCLstdlibTables.Operations._String__getSeverity /* getSeverity() */,
			OCLstdlibTables.Operations._String__indexOf /* indexOf(String[1]) */,
			OCLstdlibTables.Operations._String__lastIndexOf /* lastIndexOf(String[1]) */,
			//OCLstdlibTables.Operations._String__0_logDiagnostic /* logDiagnostic(OclAny[1],OclAny[?],OclAny[?],Integer[1],Boolean[?],Integer[1]) */,
			//OCLstdlibTables.Operations._String__1_logDiagnostic /* logDiagnostic(OclAny[1],OclAny[?],OclAny[?],OclAny[?],String[?],Integer[1],OclAny[?],Integer[1]) */,
			OCLstdlibTables.Operations._String__matches /* matches(String[1]) */,
			OCLstdlibTables.Operations._String__replaceAll /* replaceAll(String[1],String[1]) */,
			OCLstdlibTables.Operations._String__replaceFirst /* replaceFirst(String[1],String[1]) */,
			OCLstdlibTables.Operations._String__size /* size() */,
			OCLstdlibTables.Operations._String__startsWith /* startsWith(String[1]) */,
			OCLstdlibTables.Operations._String__substituteAll /* substituteAll(String[1],String[1]) */,
			OCLstdlibTables.Operations._String__substituteFirst /* substituteFirst(String[1],String[1]) */,
			OCLstdlibTables.Operations._String__substring /* substring(Integer[1],Integer[1]) */,
			OCLstdlibTables.Operations._String__toBoolean /* toBoolean() */,
			OCLstdlibTables.Operations._String__toInteger /* toInteger() */,
			OCLstdlibTables.Operations._String__toLower /* toLower() */,
			OCLstdlibTables.Operations._String__toLowerCase /* toLowerCase() */,
			OCLstdlibTables.Operations._String__toReal /* toReal() */,
			OCLstdlibTables.Operations._String__toString /* toString() */,
			OCLstdlibTables.Operations._String__toUpper /* toUpper() */,
			OCLstdlibTables.Operations._String__toUpperCase /* toUpperCase() */,
			OCLstdlibTables.Operations._String__0_tokenize /* tokenize() */,
			OCLstdlibTables.Operations._String__2_tokenize /* tokenize(String[1],Boolean[1]) */,
			OCLstdlibTables.Operations._String__1_tokenize /* tokenize(String[?]) */,
			OCLstdlibTables.Operations._String__trim /* trim() */
		};
		private static final ExecutorOperation /*@NonNull*/ [] _String__OclAny = {
			OCLstdlibTables.Operations._String___lt__gt_ /* _'<>'(OclSelf[1]) */,
			OCLstdlibTables.Operations._String___eq_ /* _'='(OclSelf[1]) */,
			OCLstdlibTables.Operations._OclAny__oclAsSet /* oclAsSet() */,
			OCLstdlibTables.Operations._OclAny__oclAsType /* oclAsType(TT)(TT[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInState /* oclIsInState(OclState[?]) */,
			OCLstdlibTables.Operations._OclAny__oclIsInvalid /* oclIsInvalid() */,
			OCLstdlibTables.Operations._OclAny__oclIsKindOf /* oclIsKindOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsNew /* oclIsNew() */,
			OCLstdlibTables.Operations._OclAny__oclIsTypeOf /* oclIsTypeOf(OclType[1]) */,
			OCLstdlibTables.Operations._OclAny__oclIsUndefined /* oclIsUndefined() */,
			OCLstdlibTables.Operations._OclAny__0_oclLog /* oclLog() */,
			OCLstdlibTables.Operations._OclAny__1_oclLog /* oclLog(String[?]) */,
			OCLstdlibTables.Operations._OclAny__oclType /* oclType() */,
			OCLstdlibTables.Operations._OclAny__oclTypes /* oclTypes() */,
			OCLstdlibTables.Operations._String__toString /* toString() */
		};
		private static final ExecutorOperation /*@NonNull*/ [] _String__OclComparable = {
			OCLstdlibTables.Operations._String___lt_ /* _'<'(OclSelf[1]) */,
			OCLstdlibTables.Operations._String___lt__eq_ /* _'<='(OclSelf[1]) */,
			OCLstdlibTables.Operations._String___gt_ /* _'>'(OclSelf[1]) */,
			OCLstdlibTables.Operations._String___gt__eq_ /* _'>='(OclSelf[1]) */,
			OCLstdlibTables.Operations._String__compareTo /* compareTo(OclSelf[1]) */
		};
		private static final ExecutorOperation /*@NonNull*/ [] _String__OclSummable = {
			OCLstdlibTables.Operations._OclSummable__sum /* sum(OclSelf[1]) */,
			OCLstdlibTables.Operations._OclSummable__zero /* zero() */
		};

		/*
		 *	Install the operation descriptors in the fragment descriptors.
		 */
		static {
			Fragments._Componentsim__Componentsim.initOperations(_Componentsim__Componentsim);
			Fragments._Componentsim__MixinBase.initOperations(_Componentsim__MixinBase);
			Fragments._Componentsim__OclAny.initOperations(_Componentsim__OclAny);
			Fragments._Componentsim__OclElement.initOperations(_Componentsim__OclElement);
			Fragments._Componentsim__Simulation.initOperations(_Componentsim__Simulation);

			Fragments._Computesim__Computesim.initOperations(_Computesim__Computesim);
			Fragments._Computesim__MixinBase.initOperations(_Computesim__MixinBase);
			Fragments._Computesim__OclAny.initOperations(_Computesim__OclAny);
			Fragments._Computesim__OclElement.initOperations(_Computesim__OclElement);
			Fragments._Computesim__Simulation.initOperations(_Computesim__Simulation);

			Fragments._Int__Int.initOperations(_Int__Int);
			Fragments._Int__OclAny.initOperations(_Int__OclAny);

			Fragments._Loadsimulator__Component.initOperations(_Loadsimulator__Component);
			Fragments._Loadsimulator__Loadsimulator.initOperations(_Loadsimulator__Loadsimulator);
			Fragments._Loadsimulator__MixinBase.initOperations(_Loadsimulator__MixinBase);
			Fragments._Loadsimulator__OclAny.initOperations(_Loadsimulator__OclAny);
			Fragments._Loadsimulator__OclElement.initOperations(_Loadsimulator__OclElement);

			Fragments._Sensorsim__MixinBase.initOperations(_Sensorsim__MixinBase);
			Fragments._Sensorsim__OclAny.initOperations(_Sensorsim__OclAny);
			Fragments._Sensorsim__OclElement.initOperations(_Sensorsim__OclElement);
			Fragments._Sensorsim__Sensorsim.initOperations(_Sensorsim__Sensorsim);
			Fragments._Sensorsim__Simulation.initOperations(_Sensorsim__Simulation);

			Fragments._Simulation__MixinBase.initOperations(_Simulation__MixinBase);
			Fragments._Simulation__OclAny.initOperations(_Simulation__OclAny);
			Fragments._Simulation__OclElement.initOperations(_Simulation__OclElement);
			Fragments._Simulation__Simulation.initOperations(_Simulation__Simulation);

			Fragments._String__OclAny.initOperations(_String__OclAny);
			Fragments._String__OclComparable.initOperations(_String__OclComparable);
			Fragments._String__OclSummable.initOperations(_String__OclSummable);
			Fragments._String__String.initOperations(_String__String);

			Init.initEnd();
		}

		/**
		 * Force initialization of the fields of WorkloadTables::FragmentOperations and all preceding sub-packages.
		 */
		public static void init() {}
	}

	/**
	 *	The lists of local properties for the local fragment of each type.
	 */
	public static class FragmentProperties {
		static {
			Init.initStart();
			FragmentOperations.init();
		}

		private static final ExecutorProperty /*@NonNull*/ [] _Componentsim = {
			WorkloadTables.Properties._Simulation__simLevel,
			WorkloadTables.Properties._Componentsim__simLifecycleTimeMax,
			WorkloadTables.Properties._Componentsim__simLifecycleTimeMin
		};

		private static final ExecutorProperty /*@NonNull*/ [] _Computesim = {
			WorkloadTables.Properties._Computesim__simImage,
			WorkloadTables.Properties._Simulation__simLevel,
			WorkloadTables.Properties._Computesim__simOptions,
			WorkloadTables.Properties._Computesim__simVirtualization
		};

		private static final ExecutorProperty /*@NonNull*/ [] _Int = {};

		private static final ExecutorProperty /*@NonNull*/ [] _Loadsimulator = {
			WorkloadTables.Properties._Loadsimulator__loadSimulatorCpu,
			WorkloadTables.Properties._Loadsimulator__loadSimulatorHdd,
			WorkloadTables.Properties._Loadsimulator__loadSimulatorHddBytes,
			WorkloadTables.Properties._Loadsimulator__loadSimulatorIo,
			WorkloadTables.Properties._Loadsimulator__loadSimulatorTimeout,
			WorkloadTables.Properties._Loadsimulator__loadSimulatorVm,
			WorkloadTables.Properties._Loadsimulator__loadSimulatorVmBytes,
			WorkloadTables.Properties._Loadsimulator__loadSimulatorVmHang,
			WorkloadTables.Properties._Loadsimulator__loadSimulatorVmKeep,
			WorkloadTables.Properties._Loadsimulator__loadSimulatorVmStride
		};

		private static final ExecutorProperty /*@NonNull*/ [] _Sensorsim = {
			WorkloadTables.Properties._Sensorsim__simChangeRate,
			WorkloadTables.Properties._Simulation__simLevel,
			WorkloadTables.Properties._Sensorsim__simMonitoringResults
		};

		private static final ExecutorProperty /*@NonNull*/ [] _Simulation = {
			WorkloadTables.Properties._Simulation__simLevel
		};

		private static final ExecutorProperty /*@NonNull*/ [] _String = {};

		/**
		 *	Install the property descriptors in the fragment descriptors.
		 */
		static {
			Fragments._Componentsim__Componentsim.initProperties(_Componentsim);
			Fragments._Computesim__Computesim.initProperties(_Computesim);
			Fragments._Int__Int.initProperties(_Int);
			Fragments._Loadsimulator__Loadsimulator.initProperties(_Loadsimulator);
			Fragments._Sensorsim__Sensorsim.initProperties(_Sensorsim);
			Fragments._Simulation__Simulation.initProperties(_Simulation);
			Fragments._String__String.initProperties(_String);

			Init.initEnd();
		}

		/**
		 * Force initialization of the fields of WorkloadTables::FragmentProperties and all preceding sub-packages.
		 */
		public static void init() {}
	}

	/**
	 *	The lists of enumeration literals for each enumeration.
	 */
	public static class EnumerationLiterals {
		static {
			Init.initStart();
			FragmentProperties.init();
		}

		/**
		 *	Install the enumeration literals in the enumerations.
		 */
		static {

			Init.initEnd();
		}

		/**
		 * Force initialization of the fields of WorkloadTables::EnumerationLiterals and all preceding sub-packages.
		 */
		public static void init() {}
	}

	/**
	 * The multiple packages above avoid problems with the Java 65536 byte limit but introduce a difficulty in ensuring that
	 * static construction occurs in the disciplined order of the packages when construction may start in any of the packages.
	 * The problem is resolved by ensuring that the static construction of each package first initializes its immediate predecessor.
	 * On completion of predecessor initialization, the residual packages are initialized by starting an initialization in the last package.
	 * This class maintains a count so that the various predecessors can distinguish whether they are the starting point and so
	 * ensure that residual construction occurs just once after all predecessors.
	 */
	private static class Init {
		/**
		 * Counter of nested static constructions. On return to zero residual construction starts. -ve once residual construction started.
		 */
		private static int initCount = 0;

		/**
		 * Invoked at the start of a static construction to defer residual cobstruction until primary constructions complete.
		 */
		private static void initStart() {
			if (initCount >= 0) {
				initCount++;
			}
		}

		/**
		 * Invoked at the end of a static construction to activate residual cobstruction once primary constructions complete.
		 */
		private static void initEnd() {
			if (initCount > 0) {
				if (--initCount == 0) {
					initCount = -1;
					EnumerationLiterals.init();
				}
			}
		}
	}

	static {
		Init.initEnd();
	}

	/*
	 * Force initialization of outer fields. Inner fields are lazily initialized.
	 */
	public static void init() {}
}
